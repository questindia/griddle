//
//  SettingViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 20/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "SettingViewController.h"
#import "UserSettingViewController.h"
#import "PATViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "ContentManager.h"
#import "ContactsViewController.h"
#import "WallScreenViewController.h"
#import "DAViewController.h"
#import "UserProfileViewController.h"
#import "MainGridViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController
{
    AppDelegate *appDelegate;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"SETTINGS";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
}

#pragma mark - Find Friends Button
- (IBAction)FindFrirendsBTN:(id)sender {
    ContactsViewController *ct = [[ContactsViewController alloc] initWithNibName:@"ContactsViewController" bundle:nil];
    
    [self.navigationController pushViewController:ct animated:YES];
}

#pragma mark - Profile Settings Button
- (IBAction)profileSettingBTN:(id)sender {
    UserSettingViewController *us = [[UserSettingViewController alloc] initWithNibName:@"UserSettingViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:us animated:YES];
}

#pragma mark - Privacy and terms button
- (IBAction)privacyPolicyBTN:(id)sender {
    PATViewController *pp = [[PATViewController alloc] initWithNibName:@"PATViewController" bundle:nil];
    
    pp.domainName =@"http://www.griddle.com/privacy.php";
    pp.typeOfContent = @"PRIVACY POLICY";
    [appDelegate.navController pushViewController:pp animated:YES];
}


- (IBAction)WallScreenButton:(id)sender {
    WallScreenViewController *ws = [[WallScreenViewController alloc] initWithNibName:@"WallScreenViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:ws animated:YES];
}

- (IBAction)LogoutBtn:(id)sender {
    
    [objManager onLogout];
    
    appDelegate.badgesCount = @"0";
    ViewController *mn = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    // Logout Timer
    appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
    
    [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
    
    appDelegate.window.rootViewController = appDelegate.navController;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - Nortification
-(IBAction)daBtn:(id)sender
{
    DAViewController *no = [[DAViewController alloc] initWithNibName:@"DAViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:no animated:YES];
}

#pragma mark - View Own Profile
- (IBAction)goToProfileView:(id)sender {
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    UserProfileViewController *profile = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    profile.userProf = userDetail;
    
    [appDelegate.navController pushViewController:profile animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

@end
