//
//  ViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 18/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"
#import "FacebookFriendLists.h"
#import "LoginViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "ContentManager.h"
#import "MainGridViewController.h"

@interface ViewController ()
{
    NSString *token;
    AppDelegate *appDelegate;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController.navigationBar setHidden:YES];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.318 green:0.698 blue:0.914 alpha:1]];
    [self.navigationController.navigationBar setHidden:TRUE];
    
    objManager = [ContentManager sharedManager];
    
    // Check fb delegates
    appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"public_profile",@"basic_info",@"user_friends",@"user_birthday",@"user_photos",@"publish_actions",@"email", nil];
        
        appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
             {
             }];
        }
    }
    
    NSDictionary *userDet = [objManager getData:@"user_details"];
    if(userDet.count > 0)
    {
        [self verifiedLogin];
    }
}

#pragma mark - Buttons
-(IBAction)facebookBtnClicked:(id)sender
{
    // get the app delegate so that we can access the session property
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen)
    {
        [appDelegate.session closeAndClearTokenInformation];
    }
    else
    {
        if (appDelegate.session.state != FBSessionStateCreated)
        {
            // Create a new, logged out session.
            NSArray *permissions = [[NSArray alloc] initWithObjects:@"public_profile",@"basic_info",@"user_friends",@"user_birthday",@"user_photos",@"publish_actions",@"email", nil];
            
            appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             // and here we make sure to update our UX according to the new session state
             [self updateView];
         }];
    }
}

-(IBAction)registerBtnClicked:(id)sender
{
    RegisterViewController *rg = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:rg animated:YES];
}
-(IBAction)loginBtnClicked:(id)sender
{
    LoginViewController *lg =[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:lg animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [appDelegate.navController.navigationBar setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Facebook login response from server
- (void)updateView
{
    // get the app delegate, so that we can reference the session property
    
    if (appDelegate.session.isOpen)
    {
        // valid account UI is shown whenever the session is open
        [facebookBtn setTitle:@"Logout" forState:UIControlStateNormal];
        
        // Facebook Access Token
        NSString *fbaccessToken = appDelegate.session.accessTokenData.accessToken;
        // User detail access with the url and access token
        NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",[fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        token = [fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
        NSURLResponse * response = nil;
        NSError * error = nil;
        
        // Contention made with facebook graph api and user details stored here
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        
        // Store user Details from mydata to dictionary with there key and values
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:Nil];
        //
         NSString *profilePic = [NSString stringWithFormat:@"https://graph.facebook.com/me/picture?access_token=%@&type=large",[fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSURL *urlPic = [NSURL URLWithString:profilePic];
        
        NSURLRequest * urlRequestPic = [NSURLRequest requestWithURL:urlPic];
        NSURLResponse * response1 = nil;
        NSError * error1 = nil;
        
        NSData * dataPic = [NSURLConnection sendSynchronousRequest:urlRequestPic returningResponse:&response1 error:&error1];
        
        // Logic For Registration Page
        RegisterViewController *fbreg = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
        
        fbreg.fbUserDictionay = dictionary;
        fbreg.fbUserProfilePic = dataPic;
        fbreg.fbuserToken = token;
        
        [appDelegate.navController pushViewController:fbreg animated:YES];
    }
    else
    {
        // login-needed account UI is shown whenever the session is closed
        [facebookBtn setTitle:@"Register with Facebook" forState:UIControlStateNormal];
    }
}

-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
    {
        [self verifiedLogin];
    }
    else
    {
        [SVProgressHUD dismiss];
        alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Verified Login
-(void)verifiedLogin
{
    MainGridViewController *mn = [[MainGridViewController alloc] initWithNibName:@"MainGridViewController" bundle:nil];
    
    appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
    
    [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
    [appDelegate.navController.navigationBar setTranslucent:NO];
    [appDelegate.navController.navigationBar setBarTintColor:[UIColor colorWithRed:0.325 green:0.698 blue:0.918 alpha:1]];
    
    appDelegate.window.rootViewController = appDelegate.navController;
}

@end
