//
//  FFViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 10/09/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "FFViewController.h"
#import "UserProfileViewController.h"
#import "SVProgressHUD.h"
#import "UIImageView+AFNetworking.h"
#import "ContentManager.h"
#import "AppDelegate.h"

@interface FFViewController ()

@end

@implementation FFViewController
{
    NSString *globalValue;
    NSMutableArray *followerList;
    AppDelegate *appDelegate;
}
@synthesize followerz, swap, fftype, userProf, userProfileData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    followerList = [[NSMutableArray alloc] init];
    
    self.navigationItem.title = fftype;
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    
    self.navigationItem.leftBarButtonItem = btnBack;
    
    ObjManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    NSDictionary *userDetail = [ObjManager getData:@"user_details"];
    
    NSDictionary *temp = @{@"user":[userDetail valueForKey:@"user"],@"pass":[userDetail valueForKey:@"pass"], @"target":[userProf valueForKey:@"uid"], @"swap":swap};
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    globalValue = @"followerTable";
    
    [webSC getFollowerListbySwap:temp controller:@"1" method:@"getfollowers.php"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - webservice call
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    if ([globalValue isEqualToString:@"followerTable"])
    {
        [SVProgressHUD dismiss];
        NSArray *gridData = [data valueForKey:@"followers"];
        
        [followerList removeAllObjects];
        for (int i = 0; i<gridData.count; i++) {
            [followerList addObject:[gridData objectAtIndex:i]];
        }
        
        [followerz reloadData];
    }
    if ([globalValue isEqualToString:@"frManager"]) {
        if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"]) {
            [SVProgressHUD dismiss];
        }
        else
        {
            [SVProgressHUD dismiss];
        }
    }
}

#pragma mark - FriendApi
- (IBAction)addRemoveFriend:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    UIImage *img = [(UIButton *)sender currentImage];
    NSLog(@"Add friend. %ld",(long)i);
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [ObjManager getData:@"user_details"];
    
    
    NSString *actionType = @"follow";
    if(img == [UIImage imageNamed:@"rriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
        actionType = @"follow";
    }
    else if (img == [UIImage imageNamed:@"addedfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
        actionType = @"unfollow";
    }
    
    
    NSArray *gridData = [userProfileData valueForKey:@"posts"];
    NSDictionary *uval  = [gridData objectAtIndex:0];
    
    NSString *ws = [uval valueForKey:@"uid"];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    long number = [[numberFormatter numberFromString:ws] longValue];
    NSNumber *no = [NSNumber numberWithLong:number];
    
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"target":no, @"action":actionType};
    globalValue = @"frManager";
    [webSC friendManager:tempCall controller:@"1" method:@"frmanage.php"];
    [SVProgressHUD dismiss];
}

- (IBAction)adRemoveFriend:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    UIImage *img = [(UIButton *)sender currentImage];
    NSLog(@"Add friend. %ld",(long)i);
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [ObjManager getData:@"user_details"];
    
    
    NSString *actionType = @"follow";
    if(img == [UIImage imageNamed:@"addfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
        actionType = @"follow";
    }
    else if (img == [UIImage imageNamed:@"addedfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
        actionType = @"unfollow";
    }
    
    NSDictionary *art = [followerList objectAtIndex:i];
    NSString *ws = [art valueForKey:@"uid"];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    long number = [[numberFormatter numberFromString:ws] longValue];
    NSNumber *no = [NSNumber numberWithLong:number];
    
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"target":no, @"action":actionType};
    globalValue = @"frManager";
    [webSC friendManager:tempCall controller:@"1" method:@"frmanage.php"];
}

#pragma mark - table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(followerList.count == 0)
        return 0;
    else
        return followerList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"FBFriends%ld",(long)indexPath.row];
    
    UITableViewCell *cell = [followerz dequeueReusableCellWithIdentifier:identifier];
    cell = nil;
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    NSDictionary *art = [followerList objectAtIndex:indexPath.row];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 180, 25)];
    [cell.contentView addSubview:textLabel];
    textLabel.text = [art valueForKey:@"n"];
    
    //user Profile redirect
    UIButton *userprof = [UIButton buttonWithType:UIButtonTypeCustom];
    userprof.frame = CGRectMake(10.0f, 2.0f, 180.0f, 40.0f);
    userprof.backgroundColor = [UIColor clearColor];
    userprof.tag = indexPath.row;
    [cell addSubview:userprof];
    [userprof addTarget:self action:@selector(userProfileView:) forControlEvents:UIControlEventTouchUpInside];
    //
    
    UILabel *detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 180, 20)];
    detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
    [cell.contentView addSubview:detailTextLabel];
    detailTextLabel.text = [art valueForKey:@"un"];
    
    
    UIImageView *imageView;
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2, 50, 50)];
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 25;
    
    imageView.tag = indexPath.row;
    
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[art valueForKey:@"pimg"]]]];
    
    [cell.contentView addSubview:imageView];
    
    // Add Remove Button
    UIButton *aRemoveFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    aRemoveFriendButton.frame = CGRectMake(260.0f, 10.0f, 50.0f, 35.0f);
    aRemoveFriendButton.backgroundColor = [UIColor clearColor];
    
    BOOL fl = [[art valueForKey:@"fl"] boolValue];
    
    
    if(fl == 0)
    {
        [aRemoveFriendButton setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
    }
    else if (fl==1)
    {
        [aRemoveFriendButton setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
    }
    
    aRemoveFriendButton.tag = indexPath.row;
    
    [cell addSubview:aRemoveFriendButton];
    
    [aRemoveFriendButton addTarget:self action:@selector(adRemoveFriend:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

#pragma mark -user profile
- (IBAction)userProfileView:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    NSLog(@"Add friend. %d",i);
    
    NSDictionary *userDetail = [followerList objectAtIndex:i];
    
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = userDetail;
    [appDelegate.navController pushViewController:userP animated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

@end
