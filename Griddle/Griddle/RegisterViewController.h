//
//  RegisterViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 19/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate,WebserviceDelegate>
{
    IBOutlet UIView *photoView;
    IBOutlet UIScrollView *scrollView;
    ContentManager *objManager;
   
}

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UITextField *emailID;
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *userPassword;
@property (strong, nonatomic) IBOutlet UITextField *userFullName;
@property (strong, nonatomic) IBOutlet UITextField *userContactNo;

// Facebook Registration
@property (strong, nonatomic) NSDictionary *fbUserDictionay;
@property (strong, nonatomic) NSData *fbUserProfilePic;
@property (strong, nonatomic) NSString *fbuserToken;

@end
