//
//  PhotoUploadViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 30/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PhotoUploadViewController.h"

@interface PhotoUploadViewController ()

@end

@implementation PhotoUploadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor clearColor];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
