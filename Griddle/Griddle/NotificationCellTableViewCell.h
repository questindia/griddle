//
//  NotificationCellTableViewCell.h
//  Griddle
//
//  Created by Sukrit Mehra on 09/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCellTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imageView1;
@property (nonatomic, strong) IBOutlet UIButton *profileViewBtn;
@property (nonatomic, strong) IBOutlet UIImageView *imageView2;
@property (nonatomic, strong) IBOutlet UIButton *nortImageBtn;
@property (nonatomic, strong) IBOutlet UILabel *details;
@property (nonatomic, strong) IBOutlet UILabel *when;

@end
