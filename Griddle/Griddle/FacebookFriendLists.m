//
//  FacebookFriendLists.m
//  Griddle
//
//  Created by Sukrit Mehra on 21/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "FacebookFriendLists.h"
#import "AppDelegate.h"
#import "AFNetworking/AFNetworking.h"
#import "SVProgressHUD.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ContentManager.h"
#import "UserProfileViewController.h"

@interface FacebookFriendLists ()

@end

@implementation FacebookFriendLists
{
    NSString *currentStatus;
    AppDelegate *appDelegate;
}
@synthesize table, friendList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Facebook Friends"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManger = [ContentManager sharedManager];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate.navController.navigationBar setHidden:NO];
}

#pragma mark - table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(friendList.count == 0)
        return 0;
    else
        return friendList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"FBFriends%d",indexPath.row];
    
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:identifier];
    cell = nil;
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    NSDictionary *art = [friendList objectAtIndex:indexPath.row];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, table.frame.size.width - 110.0f, 25)];
    [cell.contentView addSubview:textLabel];
    textLabel.text = [art valueForKey:@"n"];
    
    //user Profile redirect
    UIButton *userprof = [UIButton buttonWithType:UIButtonTypeCustom];
    userprof.frame = CGRectMake(10.0f, 2.0f, 180.0f, 40.0f);
    userprof.backgroundColor = [UIColor clearColor];
    userprof.tag = indexPath.row;
    [cell addSubview:userprof];
    [userprof addTarget:self action:@selector(userProfileView:) forControlEvents:UIControlEventTouchUpInside];
    //
    
    UILabel *detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, table.frame.size.width - 110.0f, 20)];
    detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
    [cell.contentView addSubview:detailTextLabel];
    detailTextLabel.text = [art valueForKey:@"un"];
    
    
    UIImageView *imageView;
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2, 50, 50)];
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 25;
    
    imageView.tag = indexPath.row;
    
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[art valueForKey:@"pimg"]]] placeholderImage:[UIImage imageNamed:@"facebook50x50.jpg"]];
    
    [cell.contentView addSubview:imageView];
    
    // Add Remove Button
    UIButton *addRemoveFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addRemoveFriendButton.frame = CGRectMake(table.frame.size.width - 60.0f , 10.0f, 50.0f, 35.0f);
    addRemoveFriendButton.backgroundColor = [UIColor clearColor];
    
    BOOL fl = [[art valueForKey:@"fl"] boolValue];
    
    
    if(fl == 0)
    {
        [addRemoveFriendButton setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
    }
    else if (fl==1)
    {
        [addRemoveFriendButton setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
    }
    
    
    addRemoveFriendButton.tag = indexPath.row;
    [cell addSubview:addRemoveFriendButton];
    [addRemoveFriendButton addTarget:self
                        action:@selector(addRemoveFriend:)
              forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (IBAction)addRemoveFriend:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    UIImage *img = [(UIButton *)sender currentImage];
    NSLog(@"Add friend. %d",i);
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManger getData:@"user_details"];
    
    
    NSString *actionType = @"follow";
    if(img == [UIImage imageNamed:@"addfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
        actionType = @"follow";
    }
    else if (img == [UIImage imageNamed:@"addedfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
        actionType = @"unfollow";
    }
    
    NSDictionary *art = [friendList objectAtIndex:i];
    NSString *ws = [art valueForKey:@"uid"];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    long number = [[numberFormatter numberFromString:ws] longValue];
    NSNumber *no = [NSNumber numberWithLong:number];
    
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"target":no, @"action":actionType};
    currentStatus = @"frManager";
    [webSC friendManager:tempCall controller:@"1" method:@"frmanage.php"];
}

#pragma mark - Webservice Delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    NSLog(@"%@",data);
    [SVProgressHUD dismiss];
    if([currentStatus isEqualToString:@"frManager"])
    {
        [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
        NSDictionary *userDetail = [objManger getData:@"user_details"];
        
        NSDictionary *temp = @{@"user":[userDetail valueForKey:@"user"],@"pass":[userDetail valueForKey:@"pass"]};
        
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        currentStatus = @"getFriend";
        [webSC getFriendList:temp controller:@"1" method:@"getfriends.php"];
    }
    else if ([currentStatus isEqualToString:@"getFriend"])
    {
        NSArray *gridData = [data valueForKey:@"friends"];
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        
        for (int i = 0; i<gridData.count; i++) {
            [arr addObject:[gridData objectAtIndex:i]];
        }
        
        [friendList removeAllObjects];
        friendList = arr;
        [table reloadData];
        currentStatus = @"";
    }
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark -user profile
- (IBAction)userProfileView:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    NSLog(@"Add friend. %d",i);
    
    NSDictionary *userDetail = [friendList objectAtIndex:i];
    
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = userDetail;
    [appDelegate.navController pushViewController:userP animated:YES];
    
}

@end
