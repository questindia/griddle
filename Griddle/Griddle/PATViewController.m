//
//  PATViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 25/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PATViewController.h"
#import "AppDelegate.h"

@interface PATViewController ()

@end

@implementation PATViewController
{
    UIActivityIndicatorView *spinner;
    AppDelegate *appDelegate;
}
@synthesize webView, typeOfContent, domainName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    self.webView.alpha = 0;
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.webView.delegate = self;
    NSString *urlStr;
    if([typeOfContent isEqualToString:@"PRIVACY POLICY"])
    {
        urlStr = [NSString stringWithFormat:@"%@",domainName];
        self.navigationItem.title=typeOfContent;
    }
    else if([typeOfContent isEqualToString:@"TERMS OF USE"])
    {
        urlStr = [NSString stringWithFormat:@"%@",domainName];
        self.navigationItem.title=typeOfContent;
    
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestURL];
    
    //Activity Indicator
    spinner = [[UIActivityIndicatorView alloc]                                            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setColor:[UIColor blackColor]];
    spinner.center = [[self view] center];
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Optional UIWebViewDelegate delegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [spinner removeFromSuperview];
    self.webView.alpha = 1;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.webView.alpha = 1;
    [spinner removeFromSuperview];
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

@end
