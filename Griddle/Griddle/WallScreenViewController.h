//
//  WallScreenViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 05/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ContentManager;
@interface WallScreenViewController : UIViewController
{
    ContentManager *objManager;
    
    IBOutlet UISwitch *switch1;
    IBOutlet UISwitch *switch2;
    IBOutlet UISwitch *switch3;
    
}

@property (nonatomic, strong) UISwitch *switch1;
@property (nonatomic, strong) UISwitch *switch2;
@property (nonatomic, strong) UISwitch *switch3;

@end
