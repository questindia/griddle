//
//  UserSettingViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 21/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "UserSettingViewController.h"
#import "ContentManager.h"
#import "SVProgressHUD.h"
#import "WebserviceController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

@interface UserSettingViewController ()

@end

@implementation UserSettingViewController
{
    BOOL iscamera;
    NSData *imageDataToPost;
    NSString *emailSwitch;
    NSString *mobileSwitch;
    AppDelegate *appDelegate;
}
@synthesize username, oldPassword, name, mobile, email, emailNort, mobileNoert, userProfilePic, nPasswords;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Profile";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSDictionary *tempD = [objManager getData:@"user_details"];
    
    [username setDelegate:self];
    [oldPassword setDelegate:self];
    [name setDelegate:self];
    [mobile setDelegate:self];
    [email setDelegate:self];
    
    [nPasswords setDelegate:self];
    
    userProfilePic.layer.masksToBounds = YES;
    userProfilePic.layer.cornerRadius = 35;
    
    username.text = [tempD valueForKey:@"user"];
    oldPassword.text = [tempD valueForKey:@"pass"];
    name.text = [tempD valueForKey:@"fullname"];
    mobile.text = [tempD valueForKey:@"mobile"];
    email.text = [tempD valueForKey:@"email"];
    
    mobileNoert.on = [[tempD valueForKey:@"mn"] boolValue];
    mobileSwitch = [tempD valueForKey:@"mn"];
    emailSwitch = [tempD valueForKey:@"en"];
    emailNort.on = [[tempD valueForKey:@"en"] boolValue];
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 400);
    
    
    //assume there is a memoryCache for images or videos
    userProfilePic.layer.cornerRadius = 35;
    [userProfilePic setImageWithURL:[NSURL URLWithString:[tempD valueForKey:@"pimg"]]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHideKeyboard)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointZero animated:YES];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint;
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        if(textField == name)
        {
            scrollPoint = CGPointMake(0.0, name.center.y-47);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == mobile)
        {
            scrollPoint = CGPointMake(0.0, mobile.center.y-47);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == email)
        {
            scrollPoint = CGPointMake(0.0, email.center.y-47);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == nPasswords)
        {
            scrollPoint = CGPointMake(0.0, nPasswords.center.y-47);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    else
    {
        if (textField == email)
        {
            scrollPoint = CGPointMake(0.0, email.center.y-120);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == nPasswords)
        {
            scrollPoint = CGPointMake(0.0, nPasswords.center.y-120);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

#pragma mark - Add Photo
- (IBAction)addPhoto:(id)sender {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery", nil];
    
    action.actionSheetStyle = UIActionSheetStyleDefault;
    [action showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self cameraSelected];
            iscamera = TRUE;
            break;
        case 1:
            [self photoGallerySelect];
            break;
        case 2:
            NSLog(@"Cancel");
            break;
    }
}

-(void)cameraSelected
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    @try {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Camera don't work in simulator." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    @finally {
        
    }
    
}

#pragma mark - Image Selector
-(void)photoGallerySelect
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    iscamera = FALSE;
    userProfilePic.image = image;
    userProfilePic.layer.cornerRadius = 35;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    imageDataToPost = [imageData copy];
}

#pragma mark -  Switch
-(IBAction)mb:(UISwitch *)paramSender
{
    if ([paramSender isOn]){
        [mobileNoert setOn:YES];
        mobileSwitch = @"1";
    } else {
        [mobileNoert setOn:NO];
        mobileSwitch = @"0";
    }
}

-(IBAction)emb:(UISwitch *)paramSender
{
    if ([paramSender isOn]){
        [emailNort setOn:YES];
        emailSwitch = @"1";
    } else {
        [emailNort setOn:NO];
        emailSwitch = @"0";
    }
}

#pragma mark - Save details
- (IBAction)saveDetailsBTN:(id)sender {
    [SVProgressHUD showWithStatus:@"Updating" maskType:SVProgressHUDMaskTypeGradient];
    [self tapHideKeyboard];
    NSDictionary *temp = @{
                           @"user":username.text,
                           @"pass":oldPassword.text,
                           @"email":email.text,
                           @"mobile":mobile.text,
                           @"name":name.text,
                           @"newpass":nPasswords.text,
                           @"mopt":mobileSwitch,
                           @"eopt":emailSwitch
                           };
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    [webSC userchange:temp controller:@"1" method:@"userupdate.php" imageData:imageDataToPost];
}

-(void)tapHideKeyboard
{
    [email resignFirstResponder];
    [username resignFirstResponder];
    [oldPassword resignFirstResponder];
    [nPasswords resignFirstResponder];
    [name resignFirstResponder];
    [mobile resignFirstResponder];
    [scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Webservice Delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    
    if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
    {
        [SVProgressHUD dismissWithSuccess:@"Congratulations" afterDelay:2.0];
        alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        NSDictionary *temp = [objManager getData:@"user_details"];
        NSDictionary *userDetail;
        
        if(nPasswords.text.length > 0)
        {
            userDetail = @{
                           @"user":username.text,
                           @"pass":nPasswords.text,
                           @"fullname": name.text,
                           @"email": email.text,
                           @"mobile": mobile.text,
                           @"en": emailSwitch,
                           @"mn": mobile,
                           @"pimg": [temp valueForKey:@"pimg"]
                           };
        }
        else
        {
            userDetail = @{
                           @"user":username.text,
                           @"pass":oldPassword.text,
                           @"fullname": name.text,
                           @"email": email.text,
                           @"mobile": mobile.text,
                           @"en": emailSwitch,
                           @"mn": mobile,
                           @"pimg": [temp valueForKey:@"pimg"]
                           };
        }
        
        [objManager storeData:userDetail :@"user_details"];
        
        
        [userProfilePic setImageWithURL:[NSURL URLWithString:[data valueForKey:@"pimg"]]];
    }
    else
    {
        [SVProgressHUD dismissWithError:@"Something went wrong!" afterDelay:2.0];
        alert = [[UIAlertView alloc] initWithTitle:@"Result" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

@end
