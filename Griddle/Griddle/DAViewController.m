//
//  DAViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 05/12/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "DAViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "ContentManager.h"
#import "SVProgressHUD.h"

@interface DAViewController ()

@end

@implementation DAViewController
{
    AppDelegate *appDelegate;
    WebserviceController *wenSC;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"DEACTIVATE ACCOUNT";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
    objManager = [ContentManager sharedManager];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    wenSC = [[WebserviceController alloc] init];
    wenSC.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - Button Action
-(IBAction)deleteConfirm:(id)sender
{
    [self hideKeyBoard];
    UIAlertView *alertForDelete = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to DEACTIVATE your account?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alertForDelete show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%ld",(long)buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self callDeactivateWebserver];
            break;
        case 1:
            
            break;
        default:
            break;
    }
}

#pragma mark - Hide Keyboard
-(void)hideKeyBoard
{
    [self.userName resignFirstResponder];
    [self.userPass resignFirstResponder];
}

-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - Deactivate Account
-(void)callDeactivateWebserver
{
    UIAlertView *alert;
    if (self.userName.text.length == 0 && self.userPass.text.length == 0) {
        alert = [[UIAlertView alloc] initWithTitle:@"Blank Fields" message:@"Username and Password field is empty!" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.userName.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Username field is empty!" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.userPass.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Password field is empty!" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSDictionary *temp = @{@"user":self.userName.text, @"pass":self.userPass.text};
        [wenSC deactive:temp controller:@"1" method:@"do_deactivate.php"];
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
    }
}

#pragma mark - WebserviceApi Call
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Your Account has been DEACTIVATED" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        [objManager onLogout];
        
        appDelegate.badgesCount = @"0";
        ViewController *mn = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        
        // Logout Timer
        appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
        
        [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
        
        appDelegate.window.rootViewController = appDelegate.navController;
    }
    else
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Result" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

@end
