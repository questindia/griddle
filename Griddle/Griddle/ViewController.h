//
//  ViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 18/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "WebserviceController.h"

@class ContentManager;
@interface ViewController : UIViewController <FBLoginViewDelegate, UIAlertViewDelegate, WebserviceDelegate>
{
    IBOutlet UIButton *facebookBtn;
    IBOutlet UIButton *registerBtn;
    IBOutlet UIButton *loginBtn;
    ContentManager *objManager;
}

-(IBAction)facebookBtnClicked:(id)sender;
-(IBAction)registerBtnClicked:(id)sender;
-(IBAction)loginBtnClicked:(id)sender;

@end
