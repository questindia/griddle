//
//  PhotoGalleryCell.h
//  Changing-image
//
//  Created by ignis3 on 22/05/14.
//  Copyright (c) 2014 ignis3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoGalleryCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *photoz;

@end
