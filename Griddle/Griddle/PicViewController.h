//
//  PicViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 30/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicViewController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIImageView *imageView1;
    IBOutlet UIScrollView *scroll;
}

@property(nonatomic,retain) UIImageView *imageView1;
@property (nonatomic, strong) UIImage *img;

@end
