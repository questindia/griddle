//
//  UserProfileViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 20/07/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "UserProfileViewController.h"
#import "ContentManager.h"
#import "SVProgressHUD.h"
#import "GriddleCell.h"
#import "PhotoGalleryViewController.h"
#import "UIImageView+AFNetworking.h"
#import "FFViewController.h"
#import "AppDelegate.h"

@interface UserProfileViewController ()

@end

@implementation UserProfileViewController
{
    NSString *globalValue;
    NSMutableArray *arr;
    int gridCount;
    NSTimer *timerForImageLoading;
    NSInteger checkCount;
    NSDictionary *userProfileData;
    NSNumber *points;
    NSMutableArray *followerList;
    BOOL selection;
    AppDelegate *appDelegate;
}
@synthesize userProf, collectioView,addRemoveFriendButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Profile";
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    // User profile setting
    userNameLabel.text = [userProf valueForKey:@"un"];
    userRealNameLabel.text = [userProf valueForKey:@"n"];
    followerlabel.text = @"";
    
    
    selection = NO;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    BOOL isDirectory;
    
    NSArray *a = [[userProf valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
    NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
    {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
        [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
        else
            NSLog(@"Username folder created");
    }
    
    NSArray *tempArray = [[userProf valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
    
    NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
    NSString *imgURL = [userProf valueForKey:@"pimg"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
    
    [userProfilePic setImageWithURL:[NSURL URLWithString:imgURL]];
    //
    points = @1;
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":@24, @"pages":points, @"bbid":@"", @"gid":@"", @"pid":@"", @"uid":[userProf valueForKey:@"uid"]};
    
    globalValue = @"userprofile";
    	
    [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    
    NSString *nibName = @"GriddleCell";
    UINib *nib=[UINib nibWithNibName:nibName bundle:[NSBundle mainBundle]];
    [collectioView registerNib:nib forCellWithReuseIdentifier:@"GridCell"];
    
    followerList = [[NSMutableArray alloc] init];
    arr = [[NSMutableArray alloc] init];
    gridCount = 0;
    userProfilePic.layer.masksToBounds = YES;
    userProfilePic.layer.cornerRadius = 35;
    userProfilePic.backgroundColor = [UIColor grayColor];
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
}

#pragma mark - webservice delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    NSLog(@"%@",data);
    if([globalValue isEqualToString:@"userprofile"])
    {
        userProfileData = data;
        userNameLabel.text = [userProfileData valueForKey:@"username"];
        userRealNameLabel.text = [userProfileData valueForKey:@"name"];
        followerlabel.text = [NSString stringWithFormat:@"%@",[userProfileData valueForKey:@"followers"]];
        followingLabel.text = [NSString stringWithFormat:@"%@",[userProfileData valueForKey:@"following"]];
        
        //
        addRemoveFriendButton.backgroundColor = [UIColor clearColor];
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        BOOL fl = [[userProfileData valueForKey:@"fl"] boolValue];
        
        if ([[userDetail valueForKey:@"user"] isEqualToString:[userProfileData valueForKey:@"username"]]) {
            [addRemoveFriendButton setEnabled: NO];
        }
        else
        {
            if(fl == 0)
            {
                [addRemoveFriendButton setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
            }
            else if (fl==1)
            {
                [addRemoveFriendButton setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
            }
            
            
        }
        
        [addRemoveFriendButton addTarget:self action:@selector(addRemoveFriend:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        BOOL isDirectory;
        
        NSArray *a = [[data valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
        NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
        {
            NSError *error = nil;
            NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
            if (error)
                NSLog(@"Error creating directory path: %@", [error localizedDescription]);
            else
                NSLog(@"Username folder created");
        }
        
        NSArray *tempArray = [[userProfileData valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
        
        NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
        NSString *imgURL = [userProfileData valueForKey:@"pimg"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
        
        [userProfilePic setImageWithURL:[NSURL URLWithString:imgURL]];
        
        NSArray *gridData = [userProfileData valueForKey:@"posts"];
        
        if(gridData.count > 0)
        {
            gridCount = gridData.count;
            timerForImageLoading = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(reloadCounter) userInfo:nil repeats:YES];
            
            
            int incre = [points integerValue];
            incre++;
            points = [NSNumber numberWithInt:incre];
            checkCount = 0;
            for (int i = 0; i<gridData.count; i++) {
                [arr addObject:[gridData objectAtIndex:i]];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                BOOL isDirectory;
                
                NSArray *a = [[[gridData objectAtIndex:i] valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
                NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
                {
                    NSError *error = nil;
                    NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                    [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                    if (error)
                        NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                    else
                        NSLog(@"Username folder created");
                }
                
                //assume there is a memoryCache for images or videos
                
                
                
                // Downloading Profile Picture
                NSArray *tempArray = [[[gridData objectAtIndex:i] valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
                
                NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
                NSString *imgURL = [[gridData objectAtIndex:i] valueForKey:@"pimg"];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
                
                // Download image
                NSArray *tempAr = [[[gridData objectAtIndex:i] valueForKey:@"img"] componentsSeparatedByString:@"/"];
                
                NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
                
                NSString *imgURL2 = [[gridData objectAtIndex:i] valueForKey:@"img"];
                NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
                
                // Creating User thumb image
                if(![fileManager fileExistsAtPath:writablePath]){
                    // file doesn't exist
                    NSLog(@"file doesn't exist");
                    //save Image From URL
                    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error = nil;
                        
                        [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
                        
                        if (error) {
                            NSLog(@"Error Writing File : %@",error);
                        }else{
                            NSLog(@"Image %@ Saved SuccessFully",imgName);
                        }
                    });
                }
                else{
                    // file exist
                    NSLog(@"file exist");
                }
                
                
                if(![fileManager fileExistsAtPath:writablePath2]){
                    // file doesn't exist
                    NSLog(@"file doesn't exist");
                    //save Image From URL
                    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error = nil;
                        [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                        
                        if (error) {
                            NSLog(@"Error Writing File : %@",error);
                            griddleCell.griddleImage.image = nil;
                        }else{
                            NSLog(@"Image %@ Saved SuccessFully",imgName2);
                            checkCount++;
                        }
                    });
                }
                else{
                    // file exist
                    NSLog(@"file exist");
                    checkCount++;
                    
                }
                
            }
            
        }
        
        
        if([[data valueForKey:@"return"] isEqualToString:@"ERROR"])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [SVProgressHUD dismiss];
    }
    if ([globalValue isEqualToString:@"frManager"]) {
        if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"]) {
            [SVProgressHUD dismiss];
        }
        else
        {
            [SVProgressHUD dismiss];
        }
    }
    
    if([globalValue isEqualToString:@"countIncrease"])
    {
        NSArray *gridData = [data valueForKey:@"posts"];
        
        if(gridData.count > 0)
        {
            gridCount = gridData.count;
            timerForImageLoading = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(reloadCounter) userInfo:nil repeats:YES];
            
            
            int incre = [points integerValue];
            incre++;
            points = [NSNumber numberWithInt:incre];
            checkCount = 0;
            for (int i = 0; i<gridData.count; i++) {
                [arr addObject:[gridData objectAtIndex:i]];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                BOOL isDirectory;
                
                NSArray *a = [[[gridData objectAtIndex:i] valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
                NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
                {
                    NSError *error = nil;
                    NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                    [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                    if (error)
                        NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                    else
                        NSLog(@"Username folder created");
                }
                
                //assume there is a memoryCache for images or videos
                
                
                
                // Downloading Profile Picture
                NSArray *tempArray = [[[gridData objectAtIndex:i] valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
                
                NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
                NSString *imgURL = [[gridData objectAtIndex:i] valueForKey:@"pimg"];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
                
                // Download image
                NSArray *tempAr = [[[gridData objectAtIndex:i] valueForKey:@"img"] componentsSeparatedByString:@"/"];
                
                NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
                
                NSString *imgURL2 = [[gridData objectAtIndex:i] valueForKey:@"img"];
                NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
                
                // Creating User thumb image
                if(![fileManager fileExistsAtPath:writablePath]){
                    // file doesn't exist
                    NSLog(@"file doesn't exist");
                    //save Image From URL
                    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error = nil;
                        
                        [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
                        
                        if (error) {
                            NSLog(@"Error Writing File : %@",error);
                        }else{
                            NSLog(@"Image %@ Saved SuccessFully",imgName);
                        }
                    });
                }
                else{
                    // file exist
                    NSLog(@"file exist");
                }
                
                
                if(![fileManager fileExistsAtPath:writablePath2]){
                    // file doesn't exist
                    NSLog(@"file doesn't exist");
                    //save Image From URL
                    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error = nil;
                        [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                        
                        if (error) {
                            NSLog(@"Error Writing File : %@",error);
                            griddleCell.griddleImage.image = nil;
                        }else{
                            NSLog(@"Image %@ Saved SuccessFully",imgName2);
                            checkCount++;
                        }
                    });
                }
                else{
                    // file exist
                    NSLog(@"file exist");
                    checkCount++;
                    
                }
                
            }
            
        }
        
        
        if([[data valueForKey:@"return"] isEqualToString:@"ERROR"])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [SVProgressHUD dismiss];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arr.count;
}

- (GriddleCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"GridCell";
    griddleCell = [collectioView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
        NSDictionary *art = [arr objectAtIndex:indexPath.row];
        
        if(art.count > 0)
        {
            
            //creating path to save plist file
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            BOOL isDirectory;
            
            NSArray *a = [[art valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
            NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
            
            if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
            {
                NSError *error = nil;
                NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                if (error)
                    NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                else
                    NSLog(@"Username folder created");
            }
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            
            // Download image
            NSArray *tempAr = [[art valueForKey:@"img"] componentsSeparatedByString:@"/"];
            
            NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
            
            NSString *imgURL2 = [art valueForKey:@"img"];
            NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
            
            if(![fileManager fileExistsAtPath:writablePath2]){
                // file doesn't exist
                NSLog(@"file doesn't exist");
                //save Image From URL
                NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *error = nil;
                    [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                    
                    if (error) {
                        NSLog(@"Error Writing File : %@",error);
                        griddleCell.griddleImage.image = nil;
                    }else{
                        NSLog(@"Image %@ Saved SuccessFully",imgName2);
                        
                        griddleCell.griddleImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
                    }
                });
            }
            else{
                // file exist
                NSLog(@"file exist");
                griddleCell.griddleImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
            }
            
        
            
        }
    
    griddleCell.griddleImage.hidden = NO;
    
    
    
    return griddleCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Cell Selected %ld",(long)indexPath.row);
    
    PhotoGalleryViewController *ph =[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil];
    
    ph.userDetails = [arr objectAtIndex:indexPath.row];
        
    [appDelegate.navController pushViewController:ph animated:YES];
}
#pragma mark - Collection layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.view.frame.size.width == 320) {
        return CGSizeMake(106.0f, 106.0f);
    }
    else if (self.view.frame.size.width == 375)
    {
        return CGSizeMake(124.5f, 124.5f);
    }
    else
    {
        return CGSizeMake(137.0f, 137.0f);
    }
}


-(void)reloadCounter
{
    if(checkCount == arr.count)
    {
        [timerForImageLoading invalidate];
        checkCount = 0;
        
        
        [collectioView reloadData];
        
    }
    else if (checkCount == gridCount)
    {
        [timerForImageLoading invalidate];
        checkCount = 0;
        gridCount = 0;
        
        [collectioView reloadData];
    }
}

#pragma mark - FriendApi
- (IBAction)addRemoveFriend:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    UIImage *img = [(UIButton *)sender currentImage];
    NSLog(@"Add friend. %d",i);
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    
    NSString *actionType = @"follow";
    if(img == [UIImage imageNamed:@"rriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
        actionType = @"follow";
    }
    else if (img == [UIImage imageNamed:@"addedfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
        actionType = @"unfollow";
    }
    
    
    NSArray *gridData = [userProfileData valueForKey:@"posts"];
    NSDictionary *uval  = [gridData objectAtIndex:0];
    
    NSString *ws = [uval valueForKey:@"uid"];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    long number = [[numberFormatter numberFromString:ws] longValue];
    NSNumber *no = [NSNumber numberWithLong:number];
    
    [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeGradient];
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"target":no, @"action":actionType};
    globalValue = @"frManager";
    [webSC friendManager:tempCall controller:@"1" method:@"frmanage.php"];
    [SVProgressHUD dismiss];
}

#pragma mark - scrollview
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (collectioView.contentOffset.y == collectioView.contentSize.height - scrollView.frame.size.height)
    {
        //LOAD MORE
        NSLog(@"running");
        [self paginationCall];
    }
}

-(void)paginationCall
{
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":@24, @"pages":points, @"bbid":@"", @"gid":@"", @"pid":@"", @"uid":[userProf valueForKey:@"uid"]};
    
    globalValue = @"countIncrease";
    
    [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
}

-(IBAction)seFollowerButton:(id)sender
{
    FFViewController *ffs = [[FFViewController alloc] initWithNibName:@"FFViewController" bundle:nil];
    ffs.swap = @0;
    ffs.fftype = @"Followers";
    ffs.userProf = userProf;
    ffs.userProfileData = userProfileData;
    
    [appDelegate.navController pushViewController:ffs animated:YES];
    

}

-(IBAction)seFollowersButton:(id)sender
{
    FFViewController *ffs = [[FFViewController alloc] initWithNibName:@"FFViewController" bundle:nil];
    ffs.swap = @1;
    ffs.fftype = @"Following";
    ffs.userProf = userProf;
    ffs.userProfileData = userProfileData;
    
    [appDelegate.navController pushViewController:ffs animated:YES];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

@end
