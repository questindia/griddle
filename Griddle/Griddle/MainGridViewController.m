
//
//  MainGridViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 22/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "MainGridViewController.h"
#import "GriddleCell.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "ContentManager.h"
#import "PostPicturesController.h"
#import "PhotoGalleryViewController.h"
#import "SettingViewController.h"
#import "UIKit+AFNetworking/UIImageView+AFNetworking.h"
#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>
#import "Nortification.h"

@interface MainGridViewController ()
{
    NSNumber *num;
    NSMutableArray *arr;
    NSArray *collection;
    NSDictionary *dict;
    AppDelegate *appDelegate;
    int iPhoneType;
    NSTimer *timerForImageLoading;
    NSInteger checkCount;
    NSUInteger gridCount;
    NSTimer *bomber;
    int bomberCount;
    UIBarButtonItem *rightButton;
    UIBarButtonItem *notificationButton;
    UIButton *notButton;
    UIButton *settingButton;
    UILabel *badge;
    NSTimer *timerForBadgeCount;
    UIBackgroundTaskIdentifier bgTask;
    NSInteger badgeCounter;
    NSString *globalValue;
    UIBackgroundTaskIdentifier taskId;
}

@end

@implementation MainGridViewController
{
    SystemSoundID soundClick;
}
@synthesize collectioView, poof, poof2, ani, ani2, mycheckTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"GRIDDLE";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont systemFontOfSize:20.0]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    
    [poof setHidden:YES];
    [poof2 setHidden:YES];
    
    objManager = [ContentManager sharedManager];
    appDelegate = [UIApplication sharedApplication].delegate;
    [SVProgressHUD dismissWithSuccess:@"Welcome"];
    // Do any additional setup after loading the view from its nib.
    cameraView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Footer.png"]];
    
    
    NSString *nibName = @"GriddleCell";
    UINib *nib=[UINib nibWithNibName:nibName bundle:[NSBundle mainBundle]];
    [collectioView registerNib:nib forCellWithReuseIdentifier:@"GridCell"];
    
    gridCount = 0;
    bomberCount = 0;
    
    arr = [[NSMutableArray alloc] init];
    
    
    NSNumber *screener;
    
    //
    if([UIScreen mainScreen].bounds.size.height >= 667 )
    {
        screener = [objManager getWallSize];
        
        
        if (screener == nil) {
            iPhoneType = 98;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 21;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 50;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 98;
        }
    }
    else if([UIScreen mainScreen].bounds.size.height == 568 )
    {
        screener = [objManager getWallSize];
        
        
        if (screener == nil) {
            iPhoneType = 77;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 15;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 40;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 77;
        }
    }
    else if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        screener = [objManager getWallSize];
        
        if (screener == nil) {
            iPhoneType = 63;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 12;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 30;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 63;
        }
    }
    
    for (int i=0 ; i<iPhoneType; i++) {
        dict = @{@"user":@"fbuser.png",@"bbid":[NSString stringWithFormat:@"bbidmy%d",i]};
        [arr addObject:dict];
    }
    
    [self.view addSubview:cameraView];
    
    // Multiple Image Selection
    if (![QBImagePickerController isAccessible]) {
        NSLog(@"Error: Source is not accessible.");
    }
    
    // Navigation Item
    UIImage *homeImage = [UIImage imageNamed:@"50x50_Header_Icon.png"];
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setImage:homeImage forState:UIControlStateNormal];
    homeButton.showsTouchWhenHighlighted = YES;
    homeButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    
    [homeButton addTarget:self action:@selector(griddleIconTappedNow) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    // Navigation Item on Right
    UIImage *settingImage = [UIImage imageNamed:@"Settings_Icon_58x58.png"];
    settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingButton setImage:settingImage forState:UIControlStateNormal];
    settingButton.showsTouchWhenHighlighted = YES;
    settingButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    
    [settingButton addTarget:self action:@selector(goToSettings) forControlEvents:UIControlEventTouchUpInside];
    
    rightButton = [[UIBarButtonItem alloc] initWithCustomView:settingButton];
    
    
    UIBarButtonItem *spaceFlex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceFlex.width = 15.0f;
    
    // Header notification icon
    UIImage *notiImage = [UIImage imageNamed:@"Notifications50x50px.png"];
    notButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notButton setImage:notiImage forState:UIControlStateNormal];
    notButton.showsTouchWhenHighlighted = YES;
    notButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    [notButton addTarget:self action:@selector(showNotificationView) forControlEvents:UIControlEventTouchUpInside];
    
    notificationButton = [[UIBarButtonItem alloc] initWithCustomView:notButton];
    
    NSArray *rightBtns = [[NSArray alloc] initWithObjects:rightButton, spaceFlex,notificationButton, nil];
    
    self.navigationItem.rightBarButtonItems = rightBtns;
    
    num = @1;
    
    taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(void) {
        // Uh-oh - we took too long. Stop task.
        
    }];
    [self webservercalling];
    if (taskId != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:taskId];
    }
    
    //
    // Load images
    NSArray *imageNames = @[@"an1.png", @"an2.png", @"an3.png", @"an4.png",
                            @"an5.png", @"an6.png", @"an7.png", @"an8.png",
                            @"an9.png", @"an10.png", @"an11.png", @"an12.png",
                            @"an13.png"];
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    
    
    NSArray *imageNames2 = @[@"an1.png", @"an2.png", @"an3.png", @"an4.png",
                            @"an5.png", @"an6.png", @"an7.png", @"an8.png",
                            @"an9.png", @"an10.png", @"an11.png", @"an12.png",
                            @"an13.png"];
    
    NSMutableArray *images2 = [[NSMutableArray alloc] init];
    for (int i = 0; i < imageNames2.count; i++) {
        [images2 addObject:[UIImage imageNamed:[imageNames2 objectAtIndex:i]]];
    }
    
    // Normal Animation
    ani.animationImages = images;
    ani.animationDuration = 0.2;
    
    ani2.animationImages = images2;
    ani2.animationDuration = 0.2;
    
    [ani startAnimating];
    [ani2 startAnimating];
    
    //
    //[self performSelector:@selector(selfTimerAction) withObject:nil afterDelay:5.0];
    
    // Background Proccess for Badges
    // Background nortification added for location service
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    badgeCounter= 0 ;
    [self startBackgroundTask];
    
    // Post New Pics
    appDelegate.newPost = FALSE;
    
    // Remove delay on tapping a cell
    [collectioView setDelaysContentTouches:NO];
    
}

-(void)webservercalling
{
   
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":[NSNumber numberWithInt:iPhoneType*4], @"bbid":@"", @"gid":@"", @"pid":@"",@"uid":@"",@"pages":num};
    globalValue = @"main";
    //[webSC testNewApi];
    [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arr.count;
}

- (GriddleCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"GridCell";
    griddleCell = [collectioView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if([[[arr objectAtIndex:indexPath.row] valueForKey:@"user"]isKindOfClass:[NSString class]])
    {
        griddleCell.griddleImage.backgroundColor = [UIColor whiteColor];
        griddleCell.griddleImage.image = nil;
    }
    else
    {
        NSDictionary *art = [arr objectAtIndex:indexPath.row];

        if(art.count > 0)
        {
            NSString *imgURL2 = [art valueForKey:@"img"];
                        
            [griddleCell.griddleImage setImageWithURL:[NSURL URLWithString:imgURL2]placeholderImage:[UIImage imageNamed:@"facebook50x50.jpg"]];
        }
    }
    griddleCell.griddleImage.hidden = NO;
    
    return griddleCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[[arr objectAtIndex:indexPath.row] valueForKey:@"user"]isKindOfClass:[NSString class]])
    {
        
    }
    else
    {
        PhotoGalleryViewController *ph =[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil];
    
        ph.userDetails = [arr objectAtIndex:indexPath.row];
    
        [appDelegate.navController pushViewController:ph animated:YES];
    }
}

#pragma mark - Push to Camera
-(IBAction)GoToCameraPage:(id)sender
{
    PostPicturesController *postP = [[PostPicturesController alloc] initWithNibName:@"PostPicturesController" bundle:nil];
    
    [appDelegate.navController pushViewController:postP animated:YES];
}

#pragma mark -Refresh Home
-(void)refreshSection
{
    [self webservercalling];
}

-(void)griddleIconTappedNow
{
    [collectioView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Go to Setting Controller
-(void)goToSettings
{
    SettingViewController *st =[[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:st animated:YES];
}

#pragma mark - Webservice
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    NSLog(@"%@",data);
    // New Post Visible
    
    if ([globalValue  isEqualToString:@"main"]) {
    
     NSArray *gridData = [data valueForKey:@"posts"];
    
    if(gridData.count > 0)
    {
        gridCount = gridData.count;
        if ([num isEqualToNumber:@1]) {
            [arr removeAllObjects];
        }
        long myval = [num integerValue];
        myval++;
        num = [NSNumber numberWithLong:myval];
        
        
        NSNumber *screener;
        
        //
        if([UIScreen mainScreen].bounds.size.height >= 667 )
        {
            screener = [objManager getWallSize];
            
            
            if (screener == nil) {
                iPhoneType = 98;
            }
            else if ([screener  isEqual: @3])
            {
                iPhoneType = 21;
            }
            else if ([screener isEqual:@5])
            {
                iPhoneType = 50;
            }
            else if ([screener isEqual:@7])
            {
                iPhoneType = 98;
            }
        }
        else if([UIScreen mainScreen].bounds.size.height == 568 )
        {
            screener = [objManager getWallSize];
            
            
            if (screener == nil) {
                iPhoneType = 70;
            }
            else if ([screener  isEqual: @3])
            {
                iPhoneType = 12;
            }
            else if ([screener isEqual:@5])
            {
                iPhoneType = 35;
            }
            else if ([screener isEqual:@7])
            {
                iPhoneType = 70;
            }
        }
        else if ([UIScreen mainScreen].bounds.size.height == 480)
        {
            screener = [objManager getWallSize];
            
            if (screener == nil) {
                iPhoneType = 56;
            }
            else if ([screener  isEqual: @3])
            {
                iPhoneType = 9;
            }
            else if ([screener isEqual:@5])
            {
                iPhoneType = 25;
            }
            else if ([screener isEqual:@7])
            {
                iPhoneType = 56;
            }
        }
        ;
        for (int i=0 ; i<iPhoneType; i++) {
            dict = @{@"user":@"fbuser.png",@"bbid":[NSString stringWithFormat:@"bbidmy%d",i]};
            [arr addObject:dict];
        }
        
        for(int i = 0; i<gridData.count; i++)
        {
            [arr removeLastObject];
        }
        
        checkCount = 0;
        for (int i = 0; i<gridData.count; i++)
        {
            [arr addObject:[gridData objectAtIndex:i]];
        }
        
    }
        NSMutableArray *uniqueArray = [NSMutableArray array];
        NSMutableSet *names = [NSMutableSet set];
        if (arr.count > iPhoneType*1.5) {
            [arr removeObject:dict];
        }
        for (id obj in arr) {
            NSString *destinationName = [obj valueForKey:@"bbid"];
            if (![names containsObject:destinationName])
            {
                [uniqueArray addObject:obj];
                [names addObject:destinationName];
            }
        }
        [arr removeAllObjects];
        [arr addObjectsFromArray:uniqueArray];
        
        [SVProgressHUD dismiss];
        [collectioView reloadData];
    if([[data valueForKey:@"return"] isEqualToString:@"ERROR"])
    {
       
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    }
    
    if ([globalValue isEqualToString:@"nort"]) {
        if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            appDelegate.badgesCount = [data valueForKey:@"count"];
            NSInteger bCount = [appDelegate.badgesCount integerValue];
            
            if (bCount == 0) {
                [self selfTimerRemove];
            }
            else
            {
                [self selfTimerAction:bCount];
            }
            
        }
        else
        {
        
        }
    }
    
    
}


-(void)showAnimation
{
    if (bomberCount == 0) {
        [poof setHidden:NO];
        [poof2 setHidden:NO];
    }
    
    bomberCount++;
    if (bomberCount == 5) {
        bomberCount =0;
        [poof setHidden:YES];
        [poof2 setHidden:YES];
        [bomber invalidate];
    }
}

#pragma mark - Collection layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *screener = [objManager getWallSize];
    if ([[UIScreen mainScreen]bounds].size.height == 480)
    {
        if (screener == nil) {
            return CGSizeMake(45.0f, 45.0f);
        }
        else if ([screener  isEqual: @3])
        {
            return CGSizeMake(106.0f, 106.0f);
        }
        else if ([screener isEqual:@5])
        {
            return CGSizeMake(63.7f, 73.0f);
        }
        else if ([screener isEqual:@7])
        {
            return CGSizeMake(45.0f, 45.0f);
        }
        else
        {
            return CGSizeMake(45.0f, 45.0f);
        }
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 568)
    {
        if (screener == nil) {
            return CGSizeMake(45.0f, 45.0f);
        }
        else if ([screener  isEqual: @3])
        {
            return CGSizeMake(106.0f, 106.0f);
        }
        else if ([screener isEqual:@5])
        {
            return CGSizeMake(63.7f, 64.2f);
        }
        else if ([screener isEqual:@7])
        {
            return CGSizeMake(45.0f, 45.0f);
        }
        else
        {
            return CGSizeMake(45.0f, 45.0f);
        }
    }
    else if ([[UIScreen mainScreen]bounds].size.height == 667)
    {
        if (screener == nil) {
            return CGSizeMake(52.0f, 52.0f);
        }
        else if ([screener  isEqual: @3])
        {
            return CGSizeMake(124.5f, 124.5f);
        }
        else if ([screener isEqual:@5])
        {
            return CGSizeMake(74.0f, 74.0f);
        }
        else if ([screener isEqual:@7])
        {
            return CGSizeMake(52.0f, 52.0f);
        }
        else
        {
            return CGSizeMake(52.0f, 52.0f);
        }
    }
    else
    {
        if (screener == nil) {
            return CGSizeMake(58.0f, 58.0f);
        }
        else if ([screener  isEqual: @3])
        {
            return CGSizeMake(137.0f, 137.0f);
        }
        else if ([screener isEqual:@5])
        {
            return CGSizeMake(82.0f, 82.0f);
        }
        else if ([screener isEqual:@7])
        {
            return CGSizeMake(58.0f, 58.0f);
        }
        else
        {
            return CGSizeMake(58.0f, 58.0f);
        }
    }
    
}

#pragma mark - View will appear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNumber *screener;
    
    if (appDelegate.badgesCount != nil) {
        NSInteger bCount = [appDelegate.badgesCount integerValue];
        
        if (bCount == 0) {
            [self selfTimerRemove];
        }
        else
        {
            [self selfTimerAction:bCount];
        }
    }
    
    
    //
    if([UIScreen mainScreen].bounds.size.height >= 667 )
    {
        screener = [objManager getWallSize];
        
        
        if (screener == nil) {
            iPhoneType = 98;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 21;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 50;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 98;
        }
    }
    else if([UIScreen mainScreen].bounds.size.height == 568 )
    {
        screener = [objManager getWallSize];
        
        if (screener == nil) {
            iPhoneType = 70;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 12;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 35;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 70;
        }
    }
    else if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        screener = [objManager getWallSize];
        
        if (screener == nil) {
            iPhoneType = 56;
        }
        else if ([screener  isEqual: @3])
        {
            iPhoneType = 9;
        }
        else if ([screener isEqual:@5])
        {
            iPhoneType = 25;
        }
        else if ([screener isEqual:@7])
        {
            iPhoneType = 56;
        }
    }
    [collectioView reloadData];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont systemFontOfSize:20.0]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    
    if (appDelegate.newPost) {
        appDelegate.newPost = FALSE;
        num = @1;
        [arr removeAllObjects];
        // webservice
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":[NSNumber numberWithInt:iPhoneType*4], @"bbid":@"", @"gid":@"", @"pid":@"",@"uid":@"",@"pages":num};
        
        globalValue = @"main";
        
        [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    }
}

-(void)selfTimerAction:(NSInteger)value
{
    if (badge == nil) {
        badge = [[UILabel alloc] initWithFrame:CGRectMake(settingButton.frame.size.width - 6.0, settingButton.frame.origin.y - 4.0, 15.0, 15.0)];
    }
    
    // Set the background color.
    [badge setBackgroundColor:[UIColor colorWithRed:1.0 green:91.0/255.0 blue:84.0/255.0 alpha:1.0]];
    // Set the corner radius value to make the label appear rounded.
    badge.layer.cornerRadius = 7.5;
    badge.layer.masksToBounds = YES;
    // Set its value, alignment, color and font.
    [badge setText:[NSString stringWithFormat:@"%ld",(long)value]];
    [badge setTextAlignment:NSTextAlignmentCenter];
    [badge setTextColor:[UIColor whiteColor]];
    [badge setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10.0]];
    // Add the image as a subview to the custom button.
    [notButton addSubview:badge];
}

-(void)selfTimerRemove
{
    [badge removeFromSuperview];
}

#pragma mark - Background Proccess for Badges or Notification Count
// AppDelegate Process to Run Location Service
- (void)applicationDidEnterBackground:(NSNotification *) notification
{
    [self startBackgroundTask];
}

// Start Timer in background
-(void)startBackgroundTask
{
    [self stopBackgroundTask];
    if(mycheckTime == nil)
    {
        mycheckTime = [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(checkNotification) userInfo:nil repeats:NO];
    }
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        //in case bg task is killed faster than expected, try to start Location Service
        
        if(mycheckTime == nil)
        {
            mycheckTime = [NSTimer scheduledTimerWithTimeInterval:60.0f target:self selector:@selector(checkNotification) userInfo:nil repeats:NO];
        }
    }];
}

// AppDelegate Process to Stop Location Service
- (void)applicationDidBecomeActive:(NSNotification *) notification
{
    [self stopBackgroundTask];
}

// Background timer invalidate the location service
-(void)stopBackgroundTask
{
    if(bgTask!=UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }
}


// it will call check Note function in every 60 second
-(void)checkNotification
{
    badgeCounter++;
    if (badgeCounter > 0) {
        [self checkNote];
        badgeCounter = 0;
    }
}

-(void)checkNote
{
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    if (userDetail == nil) {
        [self stopBadgeCounter];
    }
    else{
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"page":@1};
    globalValue = @"nort";
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    [webSC getnotes1:tempCall controller:@"1" method:@"getnotes.php"];
    }
}

-(void)stopBadgeCounter
{
    if ([mycheckTime isValid]) {
        [mycheckTime invalidate];
    }
    mycheckTime = nil;
    
    
}

#pragma mark - Go to Notification View
-(void)showNotificationView
{
    Nortification *nt = [[Nortification alloc] initWithNibName:@"Nortification" bundle:nil];
    
    [appDelegate.navController pushViewController:nt animated:YES];

}

#pragma mark - scrollview
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (collectioView.contentOffset.y == collectioView.contentSize.height - scrollView.frame.size.height)
    {
        //LOAD MORE
        NSLog(@"running");
        [self webservercalling];
    }
}

@end
