//
//  Nortification.m
//  Griddle
//
//  Created by Sukrit Mehra on 05/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "Nortification.h"
#import "ContentManager.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NotificationCellTableViewCell.h"
#import "UserProfileViewController.h"
#import "PhotoViewController.h"
#import "AppDelegate.h"
#import "CommentsViewController.h"

@interface Nortification ()

@end

@implementation Nortification
{
    NSString *globalValue;
    NSMutableArray *list;
    NSDictionary *tempDictionary;
    NSNumber *countNumber;
    AppDelegate *appDelegate;
}
@synthesize tableViewz;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    list = [[NSMutableArray alloc] init];
    self.navigationItem.title = @"NOTIFICATIONS";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = [UIApplication sharedApplication].delegate;
    
    appDelegate.badgesCount = @"0";
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"page":@1};
    globalValue = @"nort";
    countNumber = @1;
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    [webSC getnotes:tempCall controller:@"1" method:@"getnotes.php"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - webservice delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    NSLog(@"%@",data);
    if([globalValue isEqualToString:@"nort"])
    {
        tempDictionary = data;
        
        if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
//            if ([[data valueForKey:@"count"] isEqualToString:@"0"])
//            {
//                
//            }
//            else
//            {
                
                NSArray *tempArray = [data valueForKey:@"notes"];
                
                for (int i = 0; i < tempArray.count; i++) {
                    [list addObject:[tempArray objectAtIndex:i]];
                }
                [tableViewz reloadData];
//            }
        }
    }
    else if ([globalValue isEqualToString:@"Call"])
    {
        tempDictionary = data;
        
        if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
//            if ([[data valueForKey:@"count"] isEqualToString:@"0"])
//            {
//                
//            }
//            else
//            {
                
                NSArray *tempArray = [data valueForKey:@"notes"];
                
                for (int i = 0; i < tempArray.count; i++) {
                    [list addObject:[tempArray objectAtIndex:i]];
                    NSInteger lastSectionIndex = [tableViewz numberOfSections] - 1;
                    
                    // Then grab the number of rows in the last section
                    NSInteger lastRowIndex =list.count-1;
                    
                    // Now just construct the index path
                    NSIndexPath *pathToLastRow = [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
                    
                    [tableViewz beginUpdates];
                    [tableViewz insertRowsAtIndexPaths:[NSArray arrayWithObjects:pathToLastRow, nil] withRowAnimation:UITableViewRowAnimationNone];
                    [tableViewz endUpdates];
//                }
            }
        }
    }
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - tableview delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (list.count > 0) {
        return list.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *userData = [list objectAtIndex:indexPath.row];
    
    static NSString *identifier=@"NortCell";
    
    NSString * nib_name= @"NotificationCellTableViewCell";
    
    
    NotificationCellTableViewCell *cell_obj = [tableViewz dequeueReusableCellWithIdentifier:identifier];
    if(cell_obj==nil)
    {
        NSArray *nib=[[NSBundle mainBundle] loadNibNamed:nib_name owner:self options:nil];
        cell_obj=[nib objectAtIndex:0];
    }
    cell_obj.details.text = [userData valueForKey:@"note"];
    
    cell_obj.when.text = [userData valueForKey:@"when"];
    
    cell_obj.imageView1.layer.masksToBounds = YES;
    cell_obj.imageView1.layer.cornerRadius = 25;
    
    // User Profile Button
    cell_obj.profileViewBtn.tag = indexPath.row;
    [cell_obj.profileViewBtn addTarget:self action:@selector(userProfileView:) forControlEvents:UIControlEventTouchUpInside];
    
    //Nortification Image
    cell_obj.nortImageBtn.tag = indexPath.row;
    [cell_obj.nortImageBtn addTarget:self action:@selector(postRedirectView:) forControlEvents:UIControlEventTouchUpInside];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    BOOL isDirectory;
    
    NSArray *a = [[userData  valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
    NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
    {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
        [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
        else
            NSLog(@"Username folder created");
    }
    
    // Downloading Profile Picture
    NSArray *tempArray = [[userData valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
    
    NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
    NSString *imgURL = [userData valueForKey:@"pimg"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
    
    // Creating User thumb image
    if(![fileManager fileExistsAtPath:writablePath]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *error = nil;
            
            [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
            
            [cell_obj.imageView1 setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[userData valueForKey:@"pimg"]]] placeholderImage:[UIImage imageNamed:@"facebook50x50.jpg"]];
            
            if (error) {
                NSLog(@"Error Writing File : %@",error);
                cell_obj.imageView1.image = nil;
            }else{
                NSLog(@"Image %@ Saved SuccessFully",imgName);
                cell_obj.imageView1.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName]];
            }
        });
    }
    else{
        // file exist
        NSLog(@"file exist");
        cell_obj.imageView1.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName]];
    }
    
    if ([[userData valueForKey:@"timg"] length] > 0)
    {
        // Downloading Profile Picture
        NSArray *tempArray1 = [[userData valueForKey:@"timg"] componentsSeparatedByString:@"/"];
        
        NSString *imgName1 = [NSString stringWithFormat:@"t%@.jpg",[tempArray1 lastObject]];
        NSString *imgURL1 = [userData valueForKey:@"timg"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName1];
        
        // Creating User thumb image
        if(![fileManager fileExistsAtPath:writablePath]){
            // file doesn't exist
            NSLog(@"file doesn't exist");
            //save Image From URL
            NSData * data1 = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL1]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil;
                
                [data1 writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName1]] options:NSAtomicWrite error:&error];
                
                [cell_obj.imageView2 setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[userData valueForKey:@"timg"]]] placeholderImage:[UIImage imageNamed:@"facebook50x50.jpg"]];
                
                if (error) {
                    NSLog(@"Error Writing File : %@",error);
                    cell_obj.imageView2.backgroundColor = [UIColor whiteColor];
                }else{
                    NSLog(@"Image %@ Saved SuccessFully",imgName1);
                    cell_obj.imageView2.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName1]];
                }
            });
        }
        else{
            // file exist
            NSLog(@"file exist");
            cell_obj.imageView2.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName1]];
        }
        
    }
    return cell_obj;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [list count] - 1) //self.array is the array of items you are displaying
    {
        //If it is the last cell, Add items to your array here & update the table view
        [self getNotification];
        globalValue = @"Call";
    }
}

#pragma mark -user profile
- (IBAction)userProfileView:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    NSLog(@"Add friend. %d",i);
    
    NSDictionary *userDetail = [list objectAtIndex:i];
    
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = userDetail;
    [appDelegate.navController pushViewController:userP animated:YES];
    
}

#pragma mark -post redirect
- (IBAction)postRedirectView:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    NSLog(@"Add friend. %d",i);
    
    NSDictionary *userDetail = [list objectAtIndex:i];
    
    if ([[userDetail valueForKey:@"ntype"] isEqualToString:@"comment"]) {
//        PhotoViewController *pv = [[PhotoViewController alloc] initWithNibName:@"PhotoViewController" bundle:nil];
//        pv.userDetails = userDetail;
//        [appDelegate.navController pushViewController:pv animated:YES];
        CommentsViewController *come = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
        
        come.userPicDictionary = userDetail;
        come.type = @"pid";
        
        [appDelegate.navController pushViewController:come animated:YES];
    }
    
}

#pragma mark - calling table again
-(void)getNotification
{
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"page":countNumber};
    globalValue = @"nort";
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    [webSC getnotes:tempCall controller:@"1" method:@"getnotes.php"];
}

@end
