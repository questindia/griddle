//
//  PostPicturesController.h
//  Griddle
//
//  Created by Sukrit Mehra on 09/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "WebserviceController.h"

@class ContentManager;
@interface PostPicturesController : UIViewController <UIActionSheetDelegate, QBImagePickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,WebserviceDelegate, UITextViewDelegate, UITextFieldDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    ContentManager *objManager;
}

@property (nonatomic, strong) IBOutlet UITextView *commentTV;
@property (nonatomic, strong) IBOutlet UITextField *hashTags;
@property (nonatomic, strong) IBOutlet UITableView *tbl;
@property (nonatomic, weak) IBOutlet UIImageView *usrProfImg;

@property (nonatomic, strong) NSMutableArray *imageCollectionToPost;

@end
