//
//  CommentsViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 23/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface CommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, WebserviceDelegate, UITextViewDelegate>
{
    IBOutlet UITableView *tableViewz;
    ContentManager *objManager;
    IBOutlet UIView *commentView;
}

@property (nonatomic, strong) IBOutlet UITableView *tableViewz;
@property (nonatomic, strong) NSDictionary *userPicDictionary;
@property (nonatomic, strong) IBOutlet UITextView *commentField;
@property (nonatomic, strong) NSString *type;

@end
