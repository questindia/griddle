//
//  MainGridViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 22/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class GriddleCell, ContentManager;
@interface MainGridViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,WebserviceDelegate, UITextViewDelegate, UIScrollViewDelegate>
{
    ContentManager *objManager;
    IBOutlet UIView *cameraView;
    GriddleCell *griddleCell;
}

@property (nonatomic, strong) IBOutlet UICollectionView *collectioView;
@property (nonatomic, strong) IBOutlet UIView *poof;
@property (nonatomic, strong) IBOutlet UIView *poof2;
@property (nonatomic, strong) IBOutlet UIImageView *ani;
@property (nonatomic, strong) IBOutlet UIImageView *ani2;
@property (nonatomic, weak) NSTimer *mycheckTime;
-(void)stopBadgeCounter;

@end
