//
//  PATViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 25/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PATViewController : UIViewController <UIWebViewDelegate>
{
    UIWebView *webView;
}

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, weak) NSString *typeOfContent;
@property (nonatomic, weak) NSString *domainName;

@end
