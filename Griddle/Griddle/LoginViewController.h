//
//  LoginViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 22/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface LoginViewController : UIViewController <UITextFieldDelegate,WebserviceDelegate>
{
    ContentManager *objManager;
}

@property (nonatomic, strong) IBOutlet UITextField *username;
@property (nonatomic, strong) IBOutlet UITextField *passowrd;
@property (nonatomic, strong) IBOutlet UIButton *loginButton;

@end
