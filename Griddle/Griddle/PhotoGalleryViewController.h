//
//  PhotoGalleryViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 13/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager, PhotoGalleryCell;
@interface PhotoGalleryViewController : UIViewController <WebserviceDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    IBOutlet UIImageView *userProfilePic;
    IBOutlet UILabel *userNamez;
    ContentManager *objManager;
    PhotoGalleryCell *photoCell;
    IBOutlet UILabel *totalLikes;
    IBOutlet UILabel *totalComments;
    IBOutlet UIImageView *votedUp;
    IBOutlet UILabel *when;
}

@property (nonatomic, strong) NSDictionary *userDetails;

@property (nonatomic, strong) IBOutlet UICollectionView *photoGalleryView;

@end
