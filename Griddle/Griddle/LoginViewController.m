//
//  LoginViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 22/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "MainGridViewController.h"
#import "SVProgressHUD.h"
#import "ForgotPasswordViewController.h"
#import "ContentManager.h"

@interface LoginViewController ()
{
    NSString *deviceUDID;
    AppDelegate *appDelegate;
}

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // Do any additional setup after loading the view from its nib.
    [self.username setDelegate:self];
    [self.passowrd setDelegate:self];
    
    //Images of username and password
    UIImageView *userImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"User.png"]];
    UIImageView *passImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"passwords.png"]];
    
    //inserting space from left in uitextfield
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    userImg.frame = CGRectMake(5, 5, 20, 20);
    [spacerView addSubview:userImg];
    [self.username setLeftViewMode:UITextFieldViewModeAlways];
    [self.username setLeftView:spacerView];
    
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    passImg.frame = CGRectMake(2, 2, 26, 26);
    [spacerView2 addSubview:passImg];
    [self.passowrd setLeftViewMode:UITextFieldViewModeAlways];
    [self.passowrd setLeftView:spacerView2];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureDetected)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)tapGestureDetected
{
    [self.username resignFirstResponder];
    [self.passowrd resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate.navController.navigationBar setHidden:NO];
    [self deviceUniqueID];
}

-(void)deviceUniqueID
{
    
    deviceUDID = appDelegate.myDeviceToken;
    NSLog(@"%@",deviceUDID);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

#pragma mark - Login & Forgot password Method
-(IBAction)LoginBtnAction:(id)sender
{
    UIAlertView *alert;
    if(self.username.text.length ==0 && self.passowrd.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty!" message:@"Username and Password fields are empty." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (self.username.text.length ==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Field Empty!" message:@"Username fiels is empty." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (self.passowrd.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Field Empty!" message:@"Password is are empty." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        
        [self.username resignFirstResponder];
        [self.passowrd resignFirstResponder];
        if (deviceUDID == nil) {
            deviceUDID = @"53bc5ccd8f111dd7738a706dc4815dee920086809d16e830e775bdb92e8fefdf";
        }
        
        NSDictionary *fbDet = [objManager getFBDetails];
        
        NSString *one;
        NSString *two;
        
        if(fbDet == nil)
        {
            one = @"";
            two = @"";
        }
        else
        {
            one = [fbDet valueForKey:@"fbtoken"];
            two = [fbDet valueForKey:@"fbuid"];
        }
        
        NSDictionary *temp = @{@"username":self.username.text,@"password":self.passowrd.text,@"apple_id":deviceUDID, @"fbtoken":one, @"fbuid":two};
        [SVProgressHUD showWithStatus:@"Login in" maskType:SVProgressHUDMaskTypeGradient];
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        [webSC logincalldictionary:temp controller:@"1" method:@"userauth.php"];
    }
    
}

-(IBAction)forgotBtnAction:(id)sender
{
    ForgotPasswordViewController *fg = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    
    [appDelegate.navController pushViewController:fg animated:YES];
}

#pragma mark - Verified Login
-(void)verifiedLogin
{
    MainGridViewController *mn = [[MainGridViewController alloc] initWithNibName:@"MainGridViewController" bundle:nil];
    
    appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
    
    [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
    [appDelegate.navController.navigationBar setTranslucent:NO];
    [appDelegate.navController.navigationBar setBarTintColor:[UIColor colorWithRed:0.325 green:0.698 blue:0.918 alpha:1]];
    
    appDelegate.window.rootViewController = appDelegate.navController;
}

-(void) webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
    {
        NSDictionary *userDetail = @{
            @"user":self.username.text,
            @"pass":self.passowrd.text,
            @"fullname": [data valueForKey:@"n"],
            @"email": [data valueForKey:@"em"],
            @"mobile": [data valueForKey:@"m"],
            @"en": [data valueForKey:@"en"],
            @"mn": [data valueForKey:@"mn"],
            @"pimg": [data valueForKey:@"pimg"],
            @"uid":[data valueForKey:@"uid"]
            };
        
        [objManager storeData:userDetail :@"user_details"];
        
        
        [self verifiedLogin];
        
    }
    else
    {
        [SVProgressHUD dismiss];
        alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

@end
