//
//  DAViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 05/12/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface DAViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate,WebserviceDelegate>
{
    ContentManager *objManager;
}
@property (nonatomic,weak) IBOutlet UITextField *userName;
@property (nonatomic, weak) IBOutlet UITextField *userPass;

@end
