//
//  PhotoViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 12/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface PhotoViewController : UIViewController <WebserviceDelegate>
{
    IBOutlet UILabel *totalLikes;
    IBOutlet UILabel *totalComments;
    IBOutlet UIImageView *userImage;
    IBOutlet UIImageView *userProfilePic;
    IBOutlet UILabel *userNamez;
    ContentManager *objManager;
    IBOutlet UIImageView *votedUp;
    IBOutlet UILabel *when;
}

@property (nonatomic, strong) NSDictionary *userDetails;

@end
