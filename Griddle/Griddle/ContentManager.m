//
//  ContentManager.m
//  schudio
//
//  Created by ignis3 on 16/01/14.
//  Copyright (c) 2014 ignis2. All rights reserved.
//

#import "ContentManager.h"

@implementation ContentManager


static ContentManager *objContantManager = nil;

+(ContentManager *)sharedManager {
    if(objContantManager == Nil) {
        objContantManager = [[ContentManager alloc] init];
    }
    return objContantManager;
}

// Storing username and passaword

-(void)storeData:(id)storeObj :(NSString *)storeKey{
    @try {
        NSUserDefaults *defaultData = [NSUserDefaults standardUserDefaults];
        [defaultData setObject:storeObj forKey:storeKey];
        
        [defaultData synchronize];
       
    }
    @catch (NSException *exception) {
       
    }
}

// getting username and password
-(id)getData:(NSString *)getKey{
    id getData = nil;
     getData = [[NSUserDefaults standardUserDefaults] objectForKey:getKey];
    return getData;
}

// Storing facebook details of user
-(void)facebookUserData:(NSDictionary *)fbDetails
{
    @try {
        NSUserDefaults *defaultData = [NSUserDefaults standardUserDefaults];
        
        [defaultData setObject:fbDetails forKey:@"user_fb"];
        [defaultData synchronize];
    }
    @catch (NSException *exception) {
        
    }
}

// Getting facebook details
-(NSDictionary *)getFBDetails
{
    id getData = nil;
    getData = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_fb"];
    
    return getData;
}

// On Logout
-(void)onLogout
{
    NSUserDefaults *defaultData = [NSUserDefaults standardUserDefaults];
    [defaultData removeObjectForKey:@"user_details"];
    [defaultData removeObjectForKey:@"user_fb"];
    [defaultData synchronize];
}

#pragma mark - Stting and Getting Wall Size of User Screen Size
-(void)storeWallSize:(id)number
{
    @try {
        NSUserDefaults *defaultData = [NSUserDefaults standardUserDefaults];
        
        [defaultData setObject:number forKey:@"userScreen"];
        [defaultData synchronize];
    }
    @catch (NSException *exception) {
        
    }
}

-(id)getWallSize
{
    id getData = nil;
    getData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userScreen"];
    
    return getData;
}

@end
