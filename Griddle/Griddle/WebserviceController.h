//
//  WebserviceController.h
//  photoshare
//
//  Created by Dhiru on 26/01/14.
//  Copyright (c) 2014 ignis. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol WebserviceDelegate <NSObject>

@required
-(void) webserviceCallback: (NSDictionary *)data;

@end

@interface WebserviceController : NSObject <NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (nonatomic,strong) id<WebserviceDelegate> delegate;
@property (nonatomic, strong) NSData *receivedData;

-(void) call:(NSDictionary *)postData controller:(NSString *)controller method:(NSString *)method imageData:(NSData *)imageData;

// forgot password
-(void)forget:(NSString *)postData controller:(NSString *)controller method:(NSString *)method;

-(void)logincalldictionary:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method;

-(void)feedcalldictionary:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method;

-(void)postcall:(NSDictionary *)call controller:(NSString*)controller method:(NSString*)method;

// user update details
-(void) userchange:(NSDictionary *)postData controller:(NSString *)controller method:(NSString *)method imageData:(NSData *)imageData;

// Do vote
-(void)uservote:(NSDictionary *)voteData controller:(NSString *)controller method:(NSString *)method;
-(void)uservotes:(NSDictionary *)voteData controller:(NSString *)controller method:(NSString *)method;

// Get Comments
-(void)getComments:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method;
-(void)getCommentss:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method;

// Do Comment
-(void)userComment:(NSDictionary *)commentData controller:(NSString *)controller method:(NSString *)method;
-(void)userComments:(NSDictionary *)commentData controller:(NSString *)controller method:(NSString *)method;

// Gey friend list with fb
-(void)getFriendList:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method;

// Friend Manager to add to remove friends
-(void)friendManager:(NSDictionary *)frmanager controller:(NSString *)controller method:(NSString *)method;

// Contact Checker
-(void)contactChecker:(NSDictionary *)ckChecker controller:(NSString *)controller method:(NSString *)method;

// Get Notes
-(void)getnotes:(NSDictionary *)notes controller:(NSString *)controller method:(NSString *)method;
-(void)getnotes1:(NSDictionary *)notes controller:(NSString *)controller method:(NSString *)method;

// Get Follower
-(void)getFollowerList:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method;
-(void)getFollowerListbySwap:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method;

// See user pid/bbid Likes
-(void)getLikeByPID:(NSDictionary *)pidLikes controller:(NSString *)controller method:(NSString *)method;
-(void)getLikeByBBID:(NSDictionary *)bbidLikes controller:(NSString *)controller method:(NSString *)method;

// Account Deactivate
-(void)deactive:(NSDictionary *)deact controller:(NSString *)controller method:(NSString *)method;

@end
