//
//  Nortification.h
//  Griddle
//
//  Created by Sukrit Mehra on 05/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface Nortification : UIViewController <WebserviceDelegate, UITableViewDataSource, UITableViewDelegate>
{
    ContentManager *objManager;
    IBOutlet UITableView *tableViewz;
}

@property (nonatomic, strong) IBOutlet UITableView *tableViewz;

@end
