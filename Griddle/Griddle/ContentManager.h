//
//  ContentManager.h
//  schudio
//
//  Created by ignis3 on 16/01/14.
//  Copyright (c) 2014 ignis2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentManager:NSObject{

}

+(ContentManager *)sharedManager;

// Storing and Geting User Information
-(void)storeData:(id)storedObj :(NSString *)storeKey;
-(id)getData:(NSString *)getKey;

// Storing Facebook User-ID and AccessToken
-(void)facebookUserData:(NSDictionary *)fbDetails;
-(NSDictionary *)getFBDetails;

// User Wall Screen Size
-(void)storeWallSize:(id)number;
-(id)getWallSize;

// Remove every key after logout
-(void)onLogout;

@end