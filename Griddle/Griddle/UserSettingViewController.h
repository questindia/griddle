//
//  UserSettingViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 21/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface UserSettingViewController : UIViewController <UITextFieldDelegate, WebserviceDelegate, UIImagePickerControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>
{
    ContentManager *objManager;
    IBOutlet UIScrollView *scrollView;
}

@property (nonatomic, strong) IBOutlet UITextField *username;
@property (nonatomic, strong) IBOutlet UITextField *oldPassword;
@property (nonatomic, strong) IBOutlet UITextField *name;
@property (nonatomic, strong) IBOutlet UITextField *mobile;
@property (nonatomic, strong) IBOutlet UITextField *email;
@property (nonatomic, strong) IBOutlet UITextField *nPasswords;

@property (nonatomic, strong) IBOutlet UISwitch *emailNort;
@property (nonatomic, strong) IBOutlet UISwitch *mobileNoert;

@property (nonatomic, strong) IBOutlet UIImageView *userProfilePic;

@end
