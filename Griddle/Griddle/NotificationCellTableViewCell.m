//
//  NotificationCellTableViewCell.m
//  Griddle
//
//  Created by Sukrit Mehra on 09/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "NotificationCellTableViewCell.h"

@implementation NotificationCellTableViewCell
@synthesize imageView1, imageView2, when, details, profileViewBtn, nortImageBtn;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
