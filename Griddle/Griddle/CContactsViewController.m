//
//  ContactsViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 20/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "CContactsViewController.h"
#import "ContentManager.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "FacebookFriendLists.h"
#import <CommonCrypto/CommonDigest.h>
#import "ContactCheckerViewController.h"
#import "MainGridViewController.h"

@interface CContactsViewController ()

@end

@implementation CContactsViewController
{
    BOOL isFB;
    BOOL isContact;
    BOOL userFBUpadte;
    AppDelegate *appDelegate;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Friends"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    [btnBack setEnabled:NO];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-right.png"] style:UIBarButtonItemStylePlain target:self action:@selector(skiptomainview:)];
    self.navigationItem.rightBarButtonItem = nextBtn;
    
    objManager = [ContentManager sharedManager];
    
    isContact = FALSE;
    isFB = FALSE;
    userFBUpadte = FALSE;
    
    appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"public_profile",@"basic_info",@"user_friends",@"user_birthday",@"user_photos",@"publish_actions",@"email", nil];
        
        appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
             {
             }];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Find Contacts
- (IBAction)findContactsBTN:(id)sender
{
    CFErrorRef error = nil;
    isContact = TRUE;
    isFB = FALSE;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
    {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted)
            {
                [self loadContacts];
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        [self loadContacts];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Permission Denied" message:@"You have denied to load contact for this app" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
    
    CFRelease(addressBook);

}

-(void)loadContacts
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, nil);
    
    if (addressBook != nil)
    {
        NSLog(@"Succesful.");
        [SVProgressHUD showWithStatus:@"Accessing PhoneBook" maskType:SVProgressHUDMaskTypeGradient];
        NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
        NSMutableArray *em = [[NSMutableArray alloc] init];
        NSMutableArray *pho = [[NSMutableArray alloc] init];
        NSMutableArray *emName = [[NSMutableArray alloc] init];
        NSMutableArray *phName = [[NSMutableArray alloc] init];
        
        NSUInteger i = 0;
        for (i = 0; i < [allContacts count]; i++)
        {
            
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            NSString *firstName = (__bridge_transfer NSString
                                   *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            NSString *lastName =  (__bridge_transfer NSString
                                   *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            if(firstName.length == 0)
            {
                firstName = @"";
            }
            else if(lastName.length == 0)
            {
                lastName = @"";
            }
            NSString *fullName = [NSString stringWithFormat:@"%@ %@",
                                  firstName, lastName];
            if(fullName.length<=1)
            {
                fullName=@"";
            }
            
            if([fullName isEqualToString:@"Identified As Spam "] || [fullName isEqualToString:@"Identified A`s Spam"])
            {
                NSLog(@"Got it");
            }
            else
            {
                NSLog(@"Full Name: %@",fullName);
            
                NSString *concatStr = @"";
                //email
                ABMultiValueRef emails = ABRecordCopyValue(contactPerson, kABPersonEmailProperty);
                NSUInteger j = 0;
                for (j = 0; j < ABMultiValueGetCount(emails); j++)
                {
                    NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, j);
                    if(concatStr.length == 0)
                    {
                        concatStr = email;
                        NSString *e = [self emailmd5:email];
                        NSLog(@"Email MD5 :%@",e);
                        [em addObject:e];
                        [emName addObject:fullName];
                    }
                    else
                    {
                        NSString *e = [self emailmd5:email];
                        NSLog(@"Email MD5 :%@",e);
                        [em addObject:e];
                        [emName addObject:fullName];
                        concatStr = [NSString stringWithFormat:@"%@,%@",concatStr,email];
                    }
                    NSLog(@"Contact : %@", concatStr);
                }
            
                NSString *concatStr2 = @"";
            
                ABMutableMultiValueRef phones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
                NSUInteger k = 0;
                for(k = 0;k< ABMultiValueGetCount(phones); k++)
                {
                    NSString *phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phones, k);
                    if(concatStr2.length == 0)
                    {
                    
                        concatStr2 = [self htmlStr:phone];
                        NSString *po = [self md5:phone];
                        [pho addObject:po];
                        [phName addObject:fullName];
                        NSLog(@"MD5 : %@", [self md5:concatStr2]);
                        
                    }
                    else
                    {
                        NSString *phone2 = [self htmlStr:phone];
                        NSString *po = [self md5:phone];
                        [pho addObject:po];
                        [phName addObject:fullName];
                        NSLog(@"MD5 : %@", [self md5:phone2]);
                        concatStr2 = [NSString stringWithFormat:@"%@ , %@",concatStr2,phone2];
                    }
                
                    NSLog(@"Phones : %@", concatStr2);
                
                }
            
                CFRelease(emails);
                CFRelease(phones);
            }
        }
         CFRelease(addressBook);
        [SVProgressHUD dismiss];
        ContactCheckerViewController *ck = [[ContactCheckerViewController alloc] initWithNibName:@"ContactCheckerViewController" bundle:nil];
        
        ck.em =em;
        ck.ph = pho;
        ck.emName = emName;
        ck.phName = phName;
        
        [appDelegate.navController pushViewController:ck animated:YES];
    }

}

#pragma mark - Find friends through facebook
- (IBAction)findFBFriendsBTN:(id)sender
{
    
    NSDictionary *fbDetails = [objManager getFBDetails];
    
    if(fbDetails.count == 0)
    {
        isContact = FALSE;
        isFB = FALSE;
        // get the app delegate so that we can access the session property
        
        // this button's job is to flip-flop the session from open to closed
        if (appDelegate.session.isOpen)
        {
            [appDelegate.session closeAndClearTokenInformation];
        }
        else
        {
            if (appDelegate.session.state != FBSessionStateCreated)
            {
                // Create a new, logged out session.
                NSArray *permissions = [[NSArray alloc] initWithObjects:@"public_profile",@"basic_info",@"user_friends",@"user_birthday",@"user_photos",@"publish_actions",@"email", nil];
                
                appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
            }
            
            // if the session isn't open, let's open it now and present the login UX to the user
            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
             {
                 // and here we make sure to update our UX according to the new session state
                 [self updateView];
             }];
        }
    }
    else
    {
        isContact = FALSE;
        isFB = TRUE;
        userFBUpadte = FALSE;
        
        [SVProgressHUD showWithStatus:@"Fetching List" maskType:SVProgressHUDMaskTypeGradient];
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        NSDictionary *temp = @{@"user":[userDetail valueForKey:@"user"],@"pass":[userDetail valueForKey:@"pass"]};
        
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        
        [webSC getFriendList:temp controller:@"1" method:@"getfriends.php"];
    }
}


#pragma mark - Webservice Delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    if (isContact)
    {
        isContact = FALSE;
        isFB = FALSE;
    }
    else if (isFB)
    {
        isContact = FALSE;
        isFB = FALSE;
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            FacebookFriendLists *fb = [[FacebookFriendLists alloc] initWithNibName:@"FacebookFriendLists" bundle:nil];
            NSArray *gridData = [data valueForKey:@"friends"];
            
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            
            for (int i = 0; i<gridData.count; i++) {
                [arr addObject:[gridData objectAtIndex:i]];
            }
            
            fb.friendList = arr;
            
            [appDelegate.navController pushViewController:fb animated:YES];
        }
        else if ([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Opps!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (userFBUpadte)
    {
        isContact = FALSE;
        isFB = FALSE;
        userFBUpadte = FALSE;
        [self fbAccess];
    }
}


#pragma mark - Facebook login response from server
- (void)updateView
{
    [SVProgressHUD showWithStatus:@"Fetching List" maskType:SVProgressHUDMaskTypeGradient];
    // get the app delegate, so that we can reference the session property

    if (appDelegate.session.isOpen)
    {
        
        // Facebook Access Token
        NSString *fbaccessToken = appDelegate.session.accessTokenData.accessToken;
        // User detail access with the url and access token
        NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",[fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *token = [fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
        NSURLResponse * response = nil;
        NSError * error = nil;
        
        //        [SVProgressHUD showWithStatus:@"Accessing Details from Facebook" maskType:SVProgressHUDMaskTypeGradient];
        
        // Contention made with facebook graph api and user details stored here
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        
        // Store user Details from mydata to dictionary with there key and values
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:Nil];
        
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        
        NSDictionary *temp = @{@"user":[userDetail valueForKey:@"user"],@"pass":[userDetail valueForKey:@"pass"],@"apple_id":appDelegate.myDeviceToken, @"fbtoken":token, @"fbuid":[dictionary valueForKey:@"id"]};
        
        NSDictionary *fbDetails = @{@"fbtoken":token, @"fbuid":[dictionary valueForKey:@"id"] };
        
        [objManager facebookUserData:fbDetails];
        
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        [webSC logincalldictionary:temp controller:@"1" method:@"userauth.php"];
        userFBUpadte = TRUE;
        
    }
    else
    {
        [SVProgressHUD dismiss];
        // login-needed account UI is shown whenever the session is close
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Opps!" message:@"Please Try it again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

-(void)fbAccess
{
    isContact = FALSE;
    isFB = FALSE;
    userFBUpadte = FALSE;
    [self performSelector:@selector(findFBFriendsBTN:) withObject:nil afterDelay:0.1f];
}

#pragma mark - String Replacement and triming

-(NSString *)htmlStr:(NSString *)removedHTMLStr
{
    removedHTMLStr = [removedHTMLStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    removedHTMLStr = [removedHTMLStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
    removedHTMLStr = [removedHTMLStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    for (int i = 0; i <[removedHTMLStr length]; i++) {
        NSString *ch = [removedHTMLStr substringWithRange:NSMakeRange(i, 1)];
        [tempArr addObject:ch];
    }
    if([tempArr count] != 0)
    {
        if (tempArr.count >= 10)
        {
            int k=0;
            NSMutableString *str;
            for (int j = [tempArr count]; j >= 0; j--)
            {
                if(k==10)
                {
                    removedHTMLStr = str;
                    break;
                }
                else
                {
                    if(str.length == 0 || str== nil)
                    {
                        str = [tempArr objectAtIndex:j-1];
                        k++;
                    }
                    else
                    {
                        NSMutableString *str1 = [tempArr objectAtIndex:j-1];
                        str = [NSMutableString stringWithFormat:@"%@%@",str1,str];
                        k++;
                    }
                }
            }
        }
        else if (tempArr.count < 10)
        {
            int k=0;
            NSMutableString *str;
            for (int j = [tempArr count]; j >= 0; j--)
            {
                if(k==tempArr.count)
                {
                    removedHTMLStr = str;
                    break;
                }
                else
                {
                    if(str.length == 0 || str== nil)
                    {
                        str = [tempArr objectAtIndex:j-1];
                        k++;
                    }
                    else
                    {
                        NSMutableString *str1 = [tempArr objectAtIndex:j-1];
                        str = [NSMutableString stringWithFormat:@"%@%@",str1,str];
                        k++;
                    }
                }
            }
        }
    }
    
    return removedHTMLStr;
}

#pragma mark - Crypto
- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

- (NSString *) emailmd5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - Switch to Main View Controller
- (IBAction)skiptomainview:(id)sender {
    
    MainGridViewController *mn = [[MainGridViewController alloc] initWithNibName:@"MainGridViewController" bundle:nil];
    
    appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
    
    [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
    [appDelegate.navController.navigationBar setTranslucent:NO];
    [appDelegate.navController.navigationBar setBarTintColor:[UIColor colorWithRed:0.325 green:0.698 blue:0.918 alpha:1]];
    
    appDelegate.window.rootViewController = appDelegate.navController;
}


@end
