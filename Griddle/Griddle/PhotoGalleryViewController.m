//
//  PhotoGalleryViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 13/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PhotoGalleryViewController.h"
#import "ContentManager.h"
#import "WebserviceController.h"
#import "PhotoGalleryCell.h"
#import "PhotoViewController.h"
#import "SVProgressHUD.h"
#import "UserProfileViewController.h"
#import "CommentsViewController.h"
#import "AppDelegate.h"
#import "LikesVC.h"
#import "UIImageView+AFNetworking.h"

@interface PhotoGalleryViewController ()

@end

@implementation PhotoGalleryViewController
{
    NSMutableArray *arr;
    NSTimer *timerForImageLoading;
    NSInteger checkCount;
    BOOL checkHot;
    BOOL checkCom;
    BOOL webCall;
    UIImage *imgData;
    NSString *globValue;
    AppDelegate *appDelegate;
}
@synthesize userDetails, photoGalleryView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    arr = [[NSMutableArray alloc] init];
    
    NSString *nibName = @"PhotoGalleryCell";
    UINib *nib=[UINib nibWithNibName:nibName bundle:[NSBundle mainBundle]];
    [photoGalleryView registerNib:nib forCellWithReuseIdentifier:@"PhotoCell"];
    
    // SettingProfile Image
    self.navigationItem.title = [userDetails valueForKey:@"hashtag"];
    
    [appDelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont systemFontOfSize:20.0]}];
    
    userProfilePic.layer.masksToBounds = YES;
    userProfilePic.layer.cornerRadius = 35;
    if ([[userDetails valueForKey:@"comms"] isEqualToString:@"0"]) {
        totalComments.text = @"Comments";
    }
    else
    {
        totalComments.text = [[userDetails valueForKey:@"comms"] stringByAppendingString:@" Comments"];
    }
    if ([[userDetails valueForKey:@"hots"] isEqualToString:@"0"]) {
        totalLikes.text = @"Likes";
    }
    else
    {
        totalLikes.text = [[userDetails valueForKey:@"hots"] stringByAppendingString:@"Likes"];
    }
    
    
    checkCom = [[userDetails valueForKey:@"didcom"] boolValue];
    checkHot = [[userDetails valueForKey:@"didhot"] boolValue];
    
    when.text = [userDetails valueForKey:@"when"];
    
    votedUp.hidden = YES;
    
    userNamez.text = [userDetails valueForKey:@"n"];
   
    
    [userProfilePic setImageWithURL:[NSURL URLWithString:[userDetails valueForKey:@"pimg"]]];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // WebService for User Group of Collection Cells
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":@1, @"bbid":[userDetails valueForKey:@"bbid"], @"gid":@"", @"pid":@"",@"uid":@""};
    
    globValue = @"getPost";
    
    [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma marrk - Collectionview Delegates
#pragma mark - CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (arr.count > 0) {
        return arr.count;
    }
    else
    {
        return 0;
    }
}

-(PhotoGalleryCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"PhotoCell";
    photoCell = [photoGalleryView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary *art = [arr objectAtIndex:indexPath.row];
    
    if(art.count > 0)
    {
        
        //creating path to save plist file
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        BOOL isDirectory;
        
        NSArray *a = [[art valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
        NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
        {
            NSError *error = nil;
            NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
            if (error)
                NSLog(@"Error creating directory path: %@", [error localizedDescription]);
            else
                NSLog(@"Username folder created");
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        // Download image
        NSArray *tempAr = [[art valueForKey:@"img"] componentsSeparatedByString:@"/"];
        
        NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
        
        NSString *imgURL2 = [art valueForKey:@"img"];
        NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
        
        if(![fileManager fileExistsAtPath:writablePath2]){
            // file doesn't exist
            NSLog(@"file doesn't exist");
            //save Image From URL
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil;
                [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                
                if (error) {
                    NSLog(@"Error Writing File : %@",error);
                    photoCell.photoz.image = nil;
                }else{
                    NSLog(@"Image %@ Saved SuccessFully",imgName2);
                    
                    photoCell.photoz.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
                }
            });
        }
        else{
            // file exist
            NSLog(@"file exist");
            photoCell.photoz.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
        }
        
    }
    
    return photoCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Cell Selected %d",indexPath.row);
    
    PhotoViewController *ph =[[PhotoViewController alloc] initWithNibName:@"PhotoViewController" bundle:nil];
    
    ph.userDetails = [arr objectAtIndex:indexPath.row];
    
    [appDelegate.navController pushViewController:ph animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([arr count] == 1)
    {
        return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height);
    }
    else if ([arr count] == 2)
    {
        return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height/2);
        
    }
    else if ([arr count] == 3)
    {
        return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height/3);
    }
    else if ([arr count] == 4)
    {
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/2);
    }
    else if ([arr count] == 5)
    {
        if(indexPath.row == 2)
        {
            return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height/3);
        }
        
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/3);
    }
    else if ([arr count] == 6)
    {
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/3);
    }
    else if ([arr count] == 7)
    {
        if(indexPath.row == 6)
        {
            return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height/4);
        }
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/4);
    }
    else if ([arr count] == 8)
    {
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/4);
    }
    else if ([arr count] == 9)
    {
        if(indexPath.row == 8)
        {
            return CGSizeMake(photoGalleryView.frame.size.width, photoGalleryView.frame.size.height/5);
        }
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/5);
    }
    else if ([arr count] == 10)
    {
        return CGSizeMake(photoGalleryView.frame.size.width/2, photoGalleryView.frame.size.height/5);
    }
    
    return CGSizeMake(100, 100);
}

#pragma mark - Webservices
-(void)webserviceCallback:(NSDictionary *)data
{
    NSLog(@"%@",data);
    [SVProgressHUD dismiss];
    if([globValue isEqualToString:@"getPost"])
    {
    NSArray *gridData = [data valueForKey:@"posts"];
    
        NSArray *tArray = [data valueForKey:@"posts"];
        NSDictionary *tempD = [tArray objectAtIndex:0];
        
        if ([[data valueForKey:@"comment_count"] isEqualToString:@"0"]) {
            totalComments.text = @"Comments";
        }
        else
        {
            totalComments.text = [[data valueForKey:@"comment_count"] stringByAppendingString:@" Comments"];
        }
        
        NSString *ttl = [tempD valueForKey:@"hots"];
        
        if ([ttl isEqualToString:@"0"]) {
            totalLikes.text = @" Likes";
        }
        else
        {
            totalLikes.text = [[tempD valueForKey:@"hots"] stringByAppendingString:@" Likes"];
        }
        
        checkCom = [[tempD valueForKey:@"didcom"] boolValue];
        checkHot = [[tempD valueForKey:@"didhot"] boolValue];
    if(gridData.count > 0)
    {
        timerForImageLoading = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(reloadCounter) userInfo:nil repeats:YES];
        [arr removeAllObjects];
        for (int i = 0; i<gridData.count; i++) {
            [arr addObject:[gridData objectAtIndex:i]];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            BOOL isDirectory;
            
            NSArray *a = [[[gridData objectAtIndex:i] valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
            NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
            
            if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
            {
                NSError *error = nil;
                NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                if (error)
                    NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                else
                    NSLog(@"Username folder created");
            }
            
            //assume there is a memoryCache for images or videos
            
            
            
            // Downloading Profile Picture
            
            
            NSString *imgURL = [[gridData objectAtIndex:i] valueForKey:@"pimg"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            
            // Download image
            NSArray *tempAr = [[[gridData objectAtIndex:i] valueForKey:@"img"] componentsSeparatedByString:@"/"];
            
            NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
            
            NSString *imgURL2 = [[gridData objectAtIndex:i] valueForKey:@"img"];
            NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
            
            // Creating User thumb image
            [userProfilePic setImageWithURL:[NSURL URLWithString:imgURL]];
            
            
            if(![fileManager fileExistsAtPath:writablePath2]){
                // file doesn't exist
                NSLog(@"file doesn't exist");
                //save Image From URL
                NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *error = nil;
                    [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                    
                    if (error) {
                        NSLog(@"Error Writing File : %@",error);

                    }else{
                        NSLog(@"Image %@ Saved SuccessFully",imgName2);
                        checkCount++;
                    }
                });
            }
            else{
                // file exist
                NSLog(@"file exist");
                checkCount++;
                
            }
            
        }
    }
    
    if([[data valueForKey:@"return"] isEqualToString:@"ERROR"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    }
    else if ([globValue isEqualToString:@"voter"])
    {
        NSString *toHots;
        
        toHots = [NSString stringWithFormat:@"%@",[data valueForKey:@"hots"]];
        
        if ([toHots isEqualToString:@"0"]) {
            totalLikes.text = [NSString stringWithFormat:@"Likes"];
        }
        else
        {
            totalLikes.text = [NSString stringWithFormat:@"%@ Likes", [data valueForKey:@"hots"]];
        }
        votedUp.hidden = NO;
        votedUp.alpha = 1.0f;
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            votedUp.alpha = 0.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            votedUp.hidden = YES;
        }];
    }
    
}

-(void)reloadCounter
{
    if(checkCount == arr.count)
    {
        [timerForImageLoading invalidate];
        checkCount = 0;
        [photoGalleryView reloadData];
        
    }
}

- (IBAction)goToProfileView:(id)sender {
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = userDetails;
    [appDelegate.navController pushViewController:userP animated:YES];
}

-(IBAction)VoteBTN:(id)sender
{
    [self doVote];
}

-(void)doVote
{
    webCall = false;
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"bbid":[userDetails valueForKey:@"bbid"]};
    globValue = @"voter";
    [webSC uservotes:tempCall controller:@"1" method:@"dohot.php"];
}

-(IBAction)goToComment:(id)sender
{
    CommentsViewController *come = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
    
    come.userPicDictionary = userDetails;
    come.type = @"bbid";
    
    [appDelegate.navController pushViewController:come animated:YES];
}

#pragma mark - BBid Likes
-(IBAction)showLikes:(id)sender
{
    LikesVC *lks = [[LikesVC alloc] initWithNibName:@"LIkesVC" bundle:nil];
    lks.swap = [userDetails valueForKey:@"bbid"];
    lks.fftype = @"bbid";
    lks.userProf = userDetails;
    [appDelegate.navController pushViewController:lks animated:YES];
}


@end
