//
//  AppDelegate.h
//  Griddle
//
//  Created by Sukrit Mehra on 18/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain)  UINavigationController *navController;
@property (strong,nonatomic) FBSession *session;
@property (strong, nonatomic) NSString *myDeviceToken;
@property (strong, nonatomic) NSString *badgesCount;
@property (nonatomic) BOOL newPost;

@end
