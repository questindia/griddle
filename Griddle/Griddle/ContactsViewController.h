//
//  ContactsViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 20/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"
#import <FacebookSDK/FacebookSDK.h>

@class ContentManager;
@interface ContactsViewController : UIViewController <WebserviceDelegate>
{
    ContentManager *objManager;
}

@property (nonatomic, strong) NSString *fromView;

@end
