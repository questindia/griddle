//
//  CommentsViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 23/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "CommentsViewController.h"
#import "ContentManager.h"
#import "SVProgressHUD.h"
#import "UserProfileViewController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

@interface CommentsViewController ()

@end

@implementation CommentsViewController
{
    NSMutableArray *userComments;
    CGRect frames;
    CGRect tableFramez;
    BOOL didComment;
    AppDelegate *appDelegate;
}
@synthesize tableViewz, userPicDictionary, commentField,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    userComments = [[NSMutableArray alloc] init];
    
    commentField.delegate = self;
    
    didComment = FALSE;
    
    tableViewz.delegate =self;
    tableViewz.dataSource = self;
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    if([type isEqualToString:@"pid"])
    {
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"pid":[userPicDictionary valueForKey:@"pid"]};
    
        [webSC getComments:tempCall controller:@"1" method:@"getcomm.php"];
        [SVProgressHUD showWithStatus:@"Fetching Comments" maskType:SVProgressHUDMaskTypeGradient];
    }
    else if ([type isEqualToString:@"bbid"])
    {
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"bbid":[userPicDictionary valueForKey:@"bbid"]};
        
        [webSC getCommentss:tempCall controller:@"1" method:@"getcomm.php"];
        [SVProgressHUD showWithStatus:@"Fetching Comments" maskType:SVProgressHUDMaskTypeGradient];
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillHide)];
    
    [self.view addGestureRecognizer:tap];
    [tableViewz addGestureRecognizer:tap];
    
}

#pragma mark - Table Delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (userComments.count > 0) {
        return userComments.count;
    }
    else
    {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *userData = [userComments objectAtIndex:indexPath.row];
    
    NSString *text = [userData valueForKey:@"comment"];
    
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(240, CGFLOAT_MAX)];
    
    
    return size.height + 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"Comments%ld",(long)indexPath.row];
    
    UITableViewCell *cell = [tableViewz dequeueReusableCellWithIdentifier:identifier];
    cell = nil;
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    NSDictionary *userData = [userComments objectAtIndex:indexPath.row];
    
    // Username Display
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, tableViewz.frame.size.width-50, 30)];
    [cell.contentView addSubview:textLabel];
    textLabel.text = [userData valueForKey:@"un"];
    
    // Time stamp for posting comment
    UILabel *timeStamp = [[UILabel alloc] initWithFrame:CGRectMake(tableViewz.frame.size.width-50, 10, 40, 30)];
    timeStamp.font = [UIFont systemFontOfSize:10.0f];
    timeStamp.textAlignment = NSTextAlignmentRight;
    [cell.contentView addSubview:timeStamp];
    timeStamp.text = [userData valueForKey:@"when"];
    
    //user Profile redirect
    UIButton *userprof = [UIButton buttonWithType:UIButtonTypeCustom];
    userprof.frame = CGRectMake(10.0f, 2.0f, tableViewz.frame.size.width-130, 40.0f);
    userprof.backgroundColor = [UIColor clearColor];
    userprof.tag = indexPath.row;
    [cell addSubview:userprof];
    [userprof addTarget:self action:@selector(userProfileView:) forControlEvents:UIControlEventTouchUpInside];
    
    // Comment Display position
    NSString *text = [userData valueForKey:@"comment"];
    
    CGSize size;
    size = [text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(240, CGFLOAT_MAX)];
    
    UILabel *detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 40, tableViewz.frame.size.width-10, size.height)];
    detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
    detailTextLabel.numberOfLines = 100;
    [cell.contentView addSubview:detailTextLabel];
    detailTextLabel.text = text;
    
    UIImageView *imageView;
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 25;
    
    imageView.tag = indexPath.row;
    
    [imageView setImageWithURL:[NSURL URLWithString:[userData valueForKey:@"pimg"]]];
    
    [cell.contentView addSubview:imageView];
    return cell;
}

#pragma mark - Textfield Delegate

- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self keyboardWillHide];
        return NO;
    }
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self keyboardWillShow];
    return true;
}

#pragma mark - Keyboard hide/show
-(void)keyboardWillShow {
    // Animate the current view out of the way
    [UIView animateWithDuration:0.3f animations:^ {
        tableFramez = tableViewz.frame;
        frames = commentView.frame;
        
        
        commentView.frame = CGRectMake(0, frames.origin.y - 218, frames.size.width, frames.size.height);
        
//        [tableViewz setContentOffset:CGPointMake(0.0, commentView.frame.origin.y - 50) animated:YES];
        
        }];
}

-(void)keyboardWillHide {
    // Animate the current view back to its original position
    [commentField resignFirstResponder];
    
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        if(commentView.frame.origin.y == 376.0f)
        {
//            [tableViewz setContentOffset:CGPointZero animated:YES];
        }
        else
        {
            [UIView animateWithDuration:0.3f animations:^ {

                commentView.frame = CGRectMake(0, frames.origin.y, commentView.frame.size.width, commentView.frame.size.height);
//                [tableViewz setContentOffset:CGPointZero animated:YES];
            }];
        }
    
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        if(commentView.frame.origin.y == 464.0f)
        {
//            [tableViewz setContentOffset:CGPointZero animated:YES];
        }
        else
        {
            [UIView animateWithDuration:0.3f animations:^ {
                
                commentView.frame = CGRectMake(0, frames.origin.y, commentView.frame.size.width, commentView.frame.size.height);
//                [tableViewz setContentOffset:CGPointZero animated:YES];
            }];
        }
    }
    else if([[UIScreen mainScreen] bounds].size.height == 667)
    {
        if(commentView.frame.origin.y == 464.0f)
        {
            //            [tableViewz setContentOffset:CGPointZero animated:YES];
        }
        else
        {
            [UIView animateWithDuration:0.3f animations:^ {
                
                commentView.frame = CGRectMake(0, frames.origin.y, commentView.frame.size.width, commentView.frame.size.height);
                //                [tableViewz setContentOffset:CGPointZero animated:YES];
            }];
        }
    }
    else
    {
        if(commentView.frame.origin.y == 464.0f)
        {
            //            [tableViewz setContentOffset:CGPointZero animated:YES];
        }
        else
        {
            [UIView animateWithDuration:0.3f animations:^ {
                
                commentView.frame = CGRectMake(0, frames.origin.y, commentView.frame.size.width, commentView.frame.size.height);
                //                [tableViewz setContentOffset:CGPointZero animated:YES];
            }];
        }
    }
}


#pragma mark - Webservice delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    
    if(!didComment)
    {
        if([[data valueForKeyPath:@"return"] isEqualToString:@"SUCCESS"])
        {
            NSLog(@"%@", data);
            NSArray *arr = [data valueForKey:@"comms"];
            if(arr.count > 0)
            {
                [userComments removeAllObjects];
                for (int i=0;i<arr.count; i++) {
                    [userComments addObject:[arr objectAtIndex:i]];
                }
                [self performSelector:@selector(tableReloadz) withObject:nil afterDelay:0.2f];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
            [alert show];
        }
    }
    else if (didComment)
    {
        didComment = FALSE;
        NSLog(@"%@", data);
        [commentField setText:@""];
        [self commentFetch];
    }
}

#pragma mark - Comment
-(IBAction)doComment:(id)sender
{
    [self keyboardWillHide];
    if(commentField.text.length == 0)
    {
        
    }
    else
    {
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        didComment = TRUE;
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        if([type isEqualToString:@"pid"])
        {
            NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"pid":[userPicDictionary valueForKey:@"pid"], @"comment":commentField.text};
        
            [webSC userComment:tempCall controller:@"1" method:@"docomm.php"];
            [SVProgressHUD showWithStatus:@"Posting Comment" maskType:SVProgressHUDMaskTypeGradient];
        }
        else if ([type isEqualToString:@"bbid"])
        {
            NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"bbid":[userPicDictionary valueForKey:@"bbid"], @"comment":commentField.text};
            
            [webSC userComments:tempCall controller:@"1" method:@"docomm.php"];
            [SVProgressHUD showWithStatus:@"Posting Comment" maskType:SVProgressHUDMaskTypeGradient];
        }
    }
}

#pragma mark - Get Comment Call Again
-(void)commentFetch
{
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    if([type isEqualToString:@"pid"])
    {
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"pid":[userPicDictionary valueForKey:@"pid"]};
    
        [webSC getComments:tempCall controller:@"1" method:@"getcomm.php"];
        [SVProgressHUD showWithStatus:@"Fetching Comments" maskType:SVProgressHUDMaskTypeGradient];
    }
    else if ([type isEqualToString:@"bbid"])
    {
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"bbid":[userPicDictionary valueForKey:@"bbid"]};
        
        [webSC getCommentss:tempCall controller:@"1" method:@"getcomm.php"];
        [SVProgressHUD showWithStatus:@"Fetching Comments" maskType:SVProgressHUDMaskTypeGradient];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableReloadz
{
    [tableViewz reloadData];
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark -user profile
- (IBAction)userProfileView:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    NSLog(@"Add friend. %d",i);
    
    NSDictionary *userDetail = [userComments objectAtIndex:i];
    
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = userDetail;
    [appDelegate.navController pushViewController:userP animated:YES];
    
}

@end
