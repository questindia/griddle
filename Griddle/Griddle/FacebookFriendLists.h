//
//  FacebookFriendLists.h
//  Griddle
//
//  Created by Sukrit Mehra on 21/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface FacebookFriendLists : UIViewController <UITableViewDataSource, UITableViewDelegate, WebserviceDelegate>
{
    ContentManager *objManger;
}
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSMutableArray *friendList;

@end
