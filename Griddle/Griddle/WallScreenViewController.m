//
//  WallScreenViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 05/08/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "WallScreenViewController.h"
#import "ContentManager.h"
#import "AppDelegate.h"

@interface WallScreenViewController ()

@end

@implementation WallScreenViewController
{
    AppDelegate *appDelegate;
}
@synthesize switch1, switch2, switch3;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"WALL SIZE";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    objManager = [ContentManager sharedManager];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSNumber *screener = [objManager getWallSize];
    if (screener == nil) {
        [switch1 setOn:NO];
        [switch2 setOn:NO];
        [switch3 setOn:YES];
    }
    else if ([screener  isEqual:@3])
    {
        [switch1 setOn:YES];
        [switch2 setOn:NO];
        [switch3 setOn:NO];
    }
    else if ([screener isEqual:@5])
    {
        [switch1 setOn:NO];
        [switch2 setOn:YES];
        [switch3 setOn:NO];
    }
    else if ([screener isEqual:@7])
    {
        [switch1 setOn:NO];
        [switch2 setOn:NO];
        [switch3 setOn:YES];
    }
    else
    {
        [switch1 setOn:NO];
        [switch2 setOn:NO];
        [switch3 setOn:YES];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

-(IBAction)swtch1On:(id)sender
{
    [switch1 setOn:YES];
    [switch2 setOn:NO];
    [switch3 setOn:NO];
    [objManager storeWallSize:@3];
}

-(IBAction)swtch2On:(id)sender
{
    [switch1 setOn:NO];
    [switch2 setOn:YES];
    [switch3 setOn:NO];
    [objManager storeWallSize:@5];
}

-(IBAction)swtch3On:(id)sender
{
    [switch1 setOn:NO];
    [switch2 setOn:NO];
    [switch3 setOn:YES];
    [objManager storeWallSize:@7];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did appear");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
}

@end
