//
//  PicViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 30/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PicViewController.h"
#import "AppDelegate.h"

@interface PicViewController ()

@end

@implementation PicViewController
{
    AppDelegate *appDelegate;
}
@synthesize imageView1, img;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    imageView1.image = img;
    self.imageView1.userInteractionEnabled = YES;
    
    scroll.delegate = self;
    scroll.showsHorizontalScrollIndicator = YES;
    scroll.showsVerticalScrollIndicator = YES;
    scroll.clipsToBounds = YES;
    
    scroll.backgroundColor = [UIColor blackColor];
    
    self.imageView1.userInteractionEnabled = YES; 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    NSLog(@"delegate method called");
    return self.imageView1;
    
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

@end
