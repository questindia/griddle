//
//  UserProfileViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 20/07/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager, GriddleCell;
@interface UserProfileViewController : UIViewController <WebserviceDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>
{
    ContentManager *objManager;
    GriddleCell *griddleCell;
    IBOutlet UIImageView *userProfilePic;
    IBOutlet UILabel *userRealNameLabel;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UIButton *followerBtn;
    IBOutlet UILabel *followerlabel;
    IBOutlet UILabel *followingLabel;
    IBOutlet UIButton *followingBtn;
    IBOutlet UIButton *addRemoveFriendButton;
}

@property (nonatomic, strong) NSDictionary *userProf;
@property (nonatomic, strong) IBOutlet UICollectionView *collectioView;

@property (nonatomic, retain) UIButton *addRemoveFriendButton;

@end
