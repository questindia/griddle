//
//  RegisterViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 19/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "RegisterViewController.h"
#import "AFNetworking/AFNetworking.h"
#import "SVProgressHUD.h"
#import "ContentManager.h"
#import "AppDelegate.h"
#import "CContactsViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController
{
    NSString *encodeRecord;
    BOOL iscamera;
    NSURL *urlPath;
    NSData *imageDataToPost;
    NSString *globalString;
    NSString *deviceUDID;
    AppDelegate *appDelegate;
}
@synthesize emailID, userName, userPassword, userImage, userContactNo, userFullName, fbUserDictionay, fbUserProfilePic, fbuserToken;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    objManager = [ContentManager sharedManager];
    
    photoView.layer.cornerRadius = 35;
    photoView.layer.masksToBounds = TRUE;
    userImage.layer.masksToBounds = TRUE;
    userImage.layer.cornerRadius = 36.5;
    [photoView addSubview:userImage];
    [emailID setDelegate:self];
    [userName setDelegate:self];
    [userPassword setDelegate:self];
    [userFullName setDelegate:self];
    [userContactNo setDelegate:self];
    
    //Images of username and password
    UIImageView *userImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"User.png"]];
    UIImageView *passImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"passwords.png"]];
    UIImageView *emailImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail-24.png"]];
    UIImageView *contactImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-iphone.png"]];
    UIImageView *fullnameImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"business-card.png"]];
    
    //inserting space from left in uitextfield
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    userImg.frame = CGRectMake(5, 5, 20, 20);
    [spacerView addSubview:userImg];
    [userName setLeftViewMode:UITextFieldViewModeAlways];
    [userName setLeftView:spacerView];
    
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    passImg.frame = CGRectMake(2, 2, 26, 26);
    [spacerView2 addSubview:passImg];
    [userPassword setLeftViewMode:UITextFieldViewModeAlways];
    [userPassword setLeftView:spacerView2];
    
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    emailImg.frame = CGRectMake(5, 3, 20, 20);
    [spacerView3 addSubview:emailImg];
    [emailID setLeftViewMode:UITextFieldViewModeAlways];
    [emailID setLeftView:spacerView3];
    
    UIView *spacerView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    fullnameImg.frame = CGRectMake(2, 2, 26, 26);
    [spacerView4 addSubview:fullnameImg];
    [userFullName setLeftViewMode:UITextFieldViewModeAlways];
    [userFullName setLeftView:spacerView4];
    
    UIView *spacerView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    contactImg.frame = CGRectMake(2, 2, 26, 26);
    [spacerView5 addSubview:contactImg];
    [userContactNo setLeftViewMode:UITextFieldViewModeAlways];
    [userContactNo setLeftView:spacerView5];
    
    //Tap Gesture to scroll Down the scrollView
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHideKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    if(fbUserDictionay != nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Your information is required to sign up for Griddle App. Your information is safe with us. We maintain privacy of your information." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
        NSString *FBName = [fbUserDictionay valueForKey:@"name"];
        if(FBName != nil || FBName != (id)[NSNull null])
        {
            userFullName.text = FBName;
        }
        // FB user-id if username is not present
        
        NSString *FBUserName = [fbUserDictionay valueForKey:@"username"];
        if(FBUserName != nil || FBUserName != (id)[NSNull null])
        {
            userName.text = FBUserName;
        }
        
        NSString *FBUserEmail = [fbUserDictionay valueForKey:@"email"];
        if(FBUserEmail != nil || FBUserEmail != (id)[NSNull null])
        {
            emailID.text = FBUserEmail;
        }
        
        if(fbUserProfilePic != nil || fbUserProfilePic != (id)[NSNull null])
        {
            imageDataToPost = fbUserProfilePic;
            userImage.image = [UIImage imageWithData:fbUserProfilePic];
            userImage.layer.cornerRadius = 35;
        }
        
        NSDictionary *fbDetails = @{@"fbtoken":fbuserToken, @"fbuid":[fbUserDictionay valueForKey:@"id"] };
        
        [objManager facebookUserData:fbDetails];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.navigationController.navigationBar setHidden:false];
    [self deviceUniqueID];
}

-(void)deviceUniqueID
{
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    deviceUDID = appDelegate.myDeviceToken;
    NSLog(@"%@",deviceUDID);
}

#pragma mark - Textfield area
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointZero animated:YES];
    return [textField resignFirstResponder];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint;
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        if(textField == userPassword)
        {
            scrollPoint = CGPointMake(0.0, userPassword.center.y-17);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == userFullName)
        {
            scrollPoint = CGPointMake(0.0, userFullName.center.y-17);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
        else if (textField == userContactNo)
        {
            scrollPoint = CGPointMake(0.0, userContactNo.center.y-17);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPhoto:(id)sender {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery", nil];
    
    action.actionSheetStyle = UIActionSheetStyleDefault;
    [action showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self cameraSelected];
            iscamera = TRUE;
            break;
        case 1:
            [self photoGallerySelect];
            break;
        case 2:
            NSLog(@"Cancel");
            break;
    }
}

-(void)cameraSelected
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    @try {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Camera don't work in simulator." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    @finally {
        
    }
    
}

#pragma mark - Image Selector
-(void)photoGallerySelect
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    iscamera = FALSE;
    userImage.image = image;
    userImage.layer.cornerRadius = 35;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    imageDataToPost = [imageData copy];
}

#pragma mark - Tap to hide keyboard
-(void)tapHideKeyboard
{
    [emailID resignFirstResponder];
    [userName resignFirstResponder];
    [userPassword resignFirstResponder];
    [userFullName resignFirstResponder];
    [userContactNo resignFirstResponder];
    [scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - User Submit action
-(IBAction)userSubmitPressed:(id)sender
{
    UIAlertView *alert;
    if(userContactNo.text.length == 0 && userFullName.text.length == 0 && userName.text.length == 0 && userPassword.text.length == 0 && emailID.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty!" message:@"All fields are empty!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (emailID.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Email Field Empty!" message:@"Please fill the email field!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (userName.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Username Field Empty!" message:@"Please fill the username field!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (userPassword.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Password Field Empty!" message:@"Please fill the password field!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (userFullName.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Fullname Field Empty!" message:@"Please fill the fullname field!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if (userContactNo.text.length == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Contact Field Empty!" message:@"Please fill the contact number field!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Registering" maskType:SVProgressHUDMaskTypeGradient];
    NSDictionary *temp;
    
    if(fbuserToken.length> 0)
    {
        temp = @{@"user":userName.text, @"pass":userPassword.text, @"email":emailID.text,@"mobile":userContactNo.text, @"name":userFullName.text,@"fbtoken":fbuserToken, @"fbuid":[fbUserDictionay valueForKey:@"id"]};
    }
    else
    {
        temp = @{@"user":userName.text, @"pass":userPassword.text, @"email":emailID.text,@"mobile":userContactNo.text, @"name":userFullName.text,@"fbuid":@""};
    }
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    [webSC call:temp controller:@"1" method:@"register.php" imageData:imageDataToPost];
    globalString = @"register";
   
}

#pragma mark - Service Controller Delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    
    if ([globalString isEqualToString:@"register"]) {
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            [SVProgressHUD dismissWithSuccess:@"Congratulations" afterDelay:2.0];
            alert = [[UIAlertView alloc] initWithTitle:@"Congratulations!" message:@"Successfully registered on Griddle. You can now login" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self userLogin];
        }
        else
        {
            [SVProgressHUD dismissWithError:@"Something went wrong!" afterDelay:2.0];
            alert = [[UIAlertView alloc] initWithTitle:@"Result" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if ([globalString isEqualToString:@"logIn"])
    {
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            NSDictionary *userDetail = @{
                                         @"user":userName.text,
                                         @"pass":userPassword.text,
                                         @"fullname": [data valueForKey:@"n"],
                                         @"email": [data valueForKey:@"em"],
                                         @"mobile": [data valueForKey:@"m"],
                                         @"en": [data valueForKey:@"en"],
                                         @"mn": [data valueForKey:@"mn"],
                                         @"pimg": [data valueForKey:@"pimg"],
                                         @"uid":[data valueForKey:@"uid"]
                                         };
            
            [objManager storeData:userDetail :@"user_details"];
            
            //creating path to save plist file
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            BOOL isDirectory;
            
            NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[data valueForKey:@"un"]];
            
            if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
            {
                NSError *error = nil;
                NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                if (error)
                    NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                else
                    NSLog(@"Username folder created");
            }
            
            //assume there is a memoryCache for images or videos
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSArray *tempArray = [[data valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
                
                NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[tempArray lastObject]];
                NSString *imgURL = [data valueForKey:@"pimg"];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *writablePath = [userDirectory stringByAppendingPathComponent:imgName];
                
                if(![fileManager fileExistsAtPath:writablePath]){
                    // file doesn't exist
                    NSLog(@"file doesn't exist");
                    //save Image From URL
                    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
                    
                    NSError *error = nil;
                    [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
                    
                    if (error) {
                        NSLog(@"Error Writing File : %@",error);
                    }else{
                        NSLog(@"Image %@ Saved SuccessFully",imgName);
                    }
                }
                else{
                    // file exist
                    NSLog(@"file exist");
                }
            });
            [self verifiedLogin];
            
        }
        else
        {
            [SVProgressHUD dismiss];
            alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
}

-(void)userLogin
{
    [userName resignFirstResponder];
    [userPassword resignFirstResponder];
    if (deviceUDID == nil) {
        deviceUDID = @"53bc5ccd8f111dd7738a706dc4815dee920086809d16e830e775bdb92e8fefdf";
    }
    
    NSDictionary *fbDet = [objManager getFBDetails];
    
    NSString *one;
    NSString *two;
    
    if(fbDet == nil)
    {
        one = @"";
        two = @"";
    }
    else
    {
        one = [fbDet valueForKey:@"fbtoken"];
        two = [fbDet valueForKey:@"fbuid"];
    }
    
    NSDictionary *temp = @{@"username":userName.text,@"password":userPassword.text,@"apple_id":deviceUDID, @"fbtoken":one, @"fbuid":two};
    [SVProgressHUD showWithStatus:@"Login in" maskType:SVProgressHUDMaskTypeGradient];
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    globalString = @"logIn";
    [webSC logincalldictionary:temp controller:@"1" method:@"userauth.php"];
}

-(void)verifiedLogin
{
    
    CContactsViewController *mn = [[CContactsViewController alloc] initWithNibName:@"CContactsViewController" bundle:nil];
    
    appDelegate.navController = [[UINavigationController alloc] initWithRootViewController:mn];
    
    [appDelegate.navController.navigationBar setBarStyle:UIBarStyleDefault];
    [appDelegate.navController.navigationBar setTranslucent:NO];
    [appDelegate.navController.navigationBar setBarTintColor:[UIColor colorWithRed:0.325 green:0.698 blue:0.918 alpha:1]];
    
    appDelegate.window.rootViewController = appDelegate.navController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
