//
//  ContactCheckerViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 21/07/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@class ContentManager;
@interface ContactCheckerViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, WebserviceDelegate>
{
    ContentManager *objManger;
}
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSMutableArray *friendList;
@property (nonatomic, strong) NSMutableArray *emName;
@property (nonatomic, strong) NSMutableArray *em;
@property (nonatomic, strong) NSMutableArray *phName;
@property (nonatomic, strong) NSMutableArray *ph;

@end
