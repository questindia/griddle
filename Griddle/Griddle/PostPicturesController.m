//
//  PostPicturesController.m
//  Griddle
//
//  Created by Sukrit Mehra on 09/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PostPicturesController.h"
#import "SVProgressHUD.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import <AviarySDK/AviarySDK.h>
#import "AppDelegate.h"
#import "ContentManager.h"
#import "SDWebImage/UIImageView+WebCache.h"

#define kAFSDKDemoImageViewInset 10.0f
#define kAFSDKDemoBorderAspectRatioPortrait 3.0f/4.0f
#define kAFSDKDemoBorderAspectRatioLandscape 4.0f/3.0f

static NSString * const kAFAviaryAPIKey = @"9055028e76e4ea6e";
static NSString * const kAFAviarySecret = @"a8b63508c24dc8ed";

@interface PostPicturesController () <AVYPhotoEditorControllerDelegate, UIPopoverControllerDelegate>
{
    NSArray *collection;
    NSDictionary *dict;
    BOOL cameraTouch;
    BOOL cameraorphotogallery;
    AppDelegate *appDelegate;
    NSDictionary *userDetail;
}

@property (nonatomic, strong) ALAssetsLibrary * assetLibrary;
@property (nonatomic, strong) NSMutableArray * sessions;

@end

@implementation PostPicturesController
@synthesize commentTV, hashTags, imageCollectionToPost, usrProfImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    objManager = [ContentManager sharedManager];
    appDelegate = [UIApplication sharedApplication].delegate;
    userDetail = [objManager getData:@"user_details"];
    cameraTouch = TRUE;
    imageCollectionToPost = [[NSMutableArray alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"HeaderYellow.jpg"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName : [UIFont boldSystemFontOfSize:21.0]}];
    self.navigationItem.title = @"GRIDDLE ME THIS";
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(BackButton:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    UIBarButtonItem *pst = [[UIBarButtonItem alloc] initWithTitle:@"POST" style:UIBarButtonItemStyleDone target:self action:@selector(posttoserver:)];
    self.navigationItem.rightBarButtonItem = pst;
    // Multiple Image Selection
    if (![QBImagePickerController isAccessible]) {
        NSLog(@"Error: Source is not accessible.");
    }
    
    usrProfImg.layer.masksToBounds = YES;
    usrProfImg.layer.cornerRadius = 36;
    [usrProfImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[userDetail valueForKey:@"pimg"]]] placeholderImage:[UIImage imageNamed:@"facebook50x50.jpg"]];
    
    // Allocate Asset Library
    ALAssetsLibrary * assetLibrary = [[ALAssetsLibrary alloc] init];
    [self setAssetLibrary:assetLibrary];
    
    // Allocate Sessions Array
    NSMutableArray * sessions = [NSMutableArray new];
    [self setSessions:sessions];
    
    // Start the Aviary Editor OpenGL Load
    [AVYOpenGLManager beginOpenGLLoad];
    
    commentTV.text = @"Comment";
    commentTV.textColor = [UIColor lightGrayColor];
    
    hashTags.delegate = self;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyB)];
    
    [self.view addGestureRecognizer:tap];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    commentTV.text = @"";
    commentTV.textColor = [UIColor blackColor];
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if(commentTV.text.length == 0){
        commentTV.textColor = [UIColor lightGrayColor];
        commentTV.text = @"Comment Here!";
        [commentTV resignFirstResponder];
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma Camera
- (IBAction)CameraBtnClicked:(id)sender {
    if(imageCollectionToPost.count != 9)
    {
        cameraorphotogallery = YES;
        [self pickerSelected:@"camera"];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"You can upload 9 photo only" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(IBAction)SM:(id)sender
{
    [self singleOrMultipleAction];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            NSLog(@"Single Mode");
            cameraorphotogallery = NO;
            [self pickerSelected:@"photo"];
            break;
        case 1:
            NSLog(@"Multiple Mode");
            [self MultiplePhoto];
            break;
        case 2:
            NSLog(@"Cancelled");
            cameraTouch = TRUE;
            break;
    }
}

-(void)singleOrMultipleAction
{
    if(imageCollectionToPost.count != 9)
    {
        cameraTouch = FALSE;
        UIActionSheet *cameraAction = [[UIActionSheet alloc] initWithTitle:@"Choose" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Single Photo", @"Multiple Photos", nil];
    
        cameraAction.actionSheetStyle = UIActionSheetStyleDefault;
        [cameraAction showInView:self.view];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"You can upload 9 photo only" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)MultiplePhoto
{
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.minimumNumberOfSelection = 0;
    imagePickerController.maximumNumberOfSelection = 9 - imageCollectionToPost.count;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - UIImagePickerDelegate and method

-(void)pickerSelected:(NSString *)mode
{
    cameraTouch = FALSE;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    if ([mode isEqualToString:@"camera"]) {
        @try {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            
        }
        @catch (NSException *exception) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }
    else if([mode isEqualToString:@"photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    void(^completion)(void)  = ^(void){
        [SVProgressHUD dismiss];
        [self launchEditorWithAsset:image];
    };
    [self dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(QBImagePickerController *)imagePickerController didSelectAsset:(ALAsset *)asset
{
    NSLog(@"* imagePickerController:didSelectAsset:");
    NSLog(@"%@", asset);
    [self performSelector:@selector(tblReload) withObject:nil afterDelay:1];
    [self dismissImagePickerController];
    cameraTouch = TRUE;
}

- (void)imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    NSLog(@"** imagePickerController:didSelectAssets:");
    NSLog(@"%@", assets);
    
    for (int x=0; x< assets.count; x++) {
        ALAsset *asset = [assets objectAtIndex:x];
        ALAssetRepresentation *defaultRep = [asset defaultRepresentation];
        UIImage *imageTemp = [UIImage imageWithCGImage:[defaultRep fullScreenImage] scale:[defaultRep scale] orientation:0];
        [imageCollectionToPost addObject:imageTemp];
    }
    [self performSelector:@selector(tblReload) withObject:nil afterDelay:0.5];
    [self dismissImagePickerController];
    cameraTouch = TRUE;
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"*** imagePickerControllerDidCancel:");
    
    [self dismissImagePickerController];
    cameraTouch = TRUE;
}

- (void)dismissImagePickerController
{
    if (self.presentedViewController) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    } else {
        [appDelegate.navController popToViewController:self animated:YES];
    }
}

// Avivary sdk asset setting
#pragma mark - Photo Editor Launch Methods

- (void) launchEditorWithAsset:(UIImage *)asset
{
//    UIImage * editingResImage = [self editingResImageForAsset:asset];
//    UIImage * highResImage = [self highResImageForAsset:asset];
    
    [self launchPhotoEditorWithImage:asset highResolutionImage:asset];
}

#pragma mark - ALAssets Helper Methods

- (UIImage *)editingResImageForAsset:(ALAsset*)asset
{
    CGImageRef image = [[asset defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:image scale:1.0 orientation:UIImageOrientationUp];
}

- (UIImage *)highResImageForAsset:(ALAsset*)asset
{
    ALAssetRepresentation * representation = [asset defaultRepresentation];
    
    CGImageRef image = [representation fullResolutionImage];
    UIImageOrientation orientation = (UIImageOrientation)[representation orientation];
    CGFloat scale = [representation scale];
    
    return [UIImage imageWithCGImage:image scale:scale orientation:orientation];
}

#pragma mark - Photo Editor Creation and Presentation
- (void) launchPhotoEditorWithImage:(UIImage *)editingResImage highResolutionImage:(UIImage *)highResImage
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self setPhotoEditorCustomizationOptions];
    });
    
    
    AVYPhotoEditorController * photoEditor = [[AVYPhotoEditorController alloc] initWithImage:editingResImage];
    [photoEditor setDelegate:self];
    
    if (highResImage) {
        [self setupHighResContextForPhotoEditor:photoEditor withImage:highResImage];
    }
    
    [self presentViewController:photoEditor animated:YES completion:nil];
    
}

- (void) setupHighResContextForPhotoEditor:(AVYPhotoEditorController *)photoEditor withImage:(UIImage *)highResImage
{
    
    id<AVYPhotoEditorRender> render = [photoEditor enqueueHighResolutionRenderWithImage:highResImage
                                                                             completion:^(UIImage *result, NSError *error) {
                                                                                 if (result) {
                                                                                     UIImageWriteToSavedPhotosAlbum(result, nil, nil, NULL);
                                                                                 } else {
                                                                                     NSLog(@"High-res render failed with error : %@", error);
                                                                                 }
                                                                             }];
    
       
    [render setProgressHandler:^(CGFloat progress) {
        NSLog(@"Render now %lf percent complete", round(progress * 100.0f));
    }];
   
}

#pragma Photo Editor Delegate Methods

// This is called when the user taps "Done" in the photo editor.
- (void) photoEditor:(AVYPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    [imageCollectionToPost addObject:image];
    [self performSelector:@selector(tblReload) withObject:nil afterDelay:3];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// This is called when the user taps "Cancel" in the photo editor.
- (void) photoEditorCanceled:(AVYPhotoEditorController *)editor
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Photo Editor Customization

- (void) setPhotoEditorCustomizationOptions
{
    // Set API Key and Secret
    [AVYPhotoEditorController setAPIKey:kAFAviaryAPIKey secret:kAFAviarySecret];
    
    // Set Tool Order
    NSArray *toolOrder = @[kAVYEffects,
                           kAVYFocus,
                           kAVYFrames,
                           kAVYStickers,
                           kAVYEnhance,
                           kAVYOrientation,
                           kAVYCrop,
                           kAVYColorAdjust,
                           kAVYLightingAdjust,
                           kAVYSplash,
                           kAVYDraw,
                           kAVYText,
                           kAVYRedeye,
                           kAVYWhiten,
                           kAVYBlemish,
                           kAVYMeme];
    [AVYPhotoEditorCustomization setToolOrder:toolOrder];
    
    // Set Custom Crop Sizes
    [AVYPhotoEditorCustomization setCropToolOriginalEnabled:NO];
    [AVYPhotoEditorCustomization setCropToolCustomEnabled:YES];
    NSDictionary *fourBySix = @{kAVYCropPresetHeight : @4.0f,
                                kAVYCropPresetWidth : @6.0f};
    NSDictionary *fiveBySeven = @{kAVYCropPresetHeight : @5.0f,
                                  kAVYCropPresetWidth : @7.0f};
    NSDictionary *square = @{kAVYCropPresetName: @"Square",
                             kAVYCropPresetHeight : @1.0f,
                             kAVYCropPresetWidth : @1.0f};
    
    [AVYPhotoEditorCustomization setCropToolPresets:@[fourBySix,
                                                      fiveBySeven,
                                                      square]];
    
    // Set Supported Orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSArray *supportedOrientations = @[@(UIInterfaceOrientationPortrait),
                                           @(UIInterfaceOrientationPortraitUpsideDown),
                                           @(UIInterfaceOrientationLandscapeLeft),
                                           @(UIInterfaceOrientationLandscapeRight)];
        [AVYPhotoEditorCustomization setSupportedIpadOrientations:supportedOrientations];
    }
}

#pragma mark - Private Helper Methods

- (BOOL) hasValidAPIKey
{
    if ([kAFAviaryAPIKey isEqualToString:@"9055028e76e4ea6e"] || [kAFAviarySecret isEqualToString:@"a8b63508c24dc8ed"]) {
        [[[UIAlertView alloc] initWithTitle:@"Oops!"
                                    message:@"You forgot to add your API key and secret!"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}
#pragma mark - Webservice
-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    
    if([[data valueForKey:@"return"] isEqualToString:@"ERROR"] || data == nil)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Your photo's uploaded successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        appDelegate.newPost = TRUE;
        [alert show];
    }
}


#pragma mark - Click to post

- (IBAction)posttoserver:(id)sender {
    
    if(hashTags.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No hashtag Found" message:@"Please provide hashtag for the photos, like - #random, #nature etc." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
    
    WebserviceController *web = [[WebserviceController alloc] init];
    web.delegate =self;
    
    
    NSString *str;
    if([commentTV.text isEqualToString:@"Comment Here!"])
    {
        str = @"";
    }
    else
    {
        str = commentTV.text;
    }
    
    NSDictionary *temp = @{@"username":[userDetail valueForKey:@"user"] , @"password":[userDetail valueForKey:@"pass"], @"sel_grid":@"",@"typed_grid":hashTags.text ,@"message":str, @"pic":imageCollectionToPost};
    
    [web postcall:temp controller:@"1" method:@"post.php"];
    [SVProgressHUD showWithStatus:@"Uploading Photos" maskType:SVProgressHUDMaskTypeGradient];
}


-(IBAction)BackButton:(id)sender
{
    [appDelegate.navController.navigationBar setHidden:NO];
    [appDelegate.navController popViewControllerAnimated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self performSelector:@selector(BackButton:) withObject:nil afterDelay:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return imageCollectionToPost.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"POST%ld",(long)indexPath.row];
    
    UITableViewCell *cell = [self.tbl dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(30.0, 60.0, 240.0, 240.0)];
    imageView.image = [imageCollectionToPost objectAtIndex:indexPath.row];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    [cell.contentView addSubview:imageView];
    
    UIButton *crossButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    crossButton.frame = CGRectMake(imageView.frame.size.width+15, 60.0-15.0, 30.0f, 30.0f);
    crossButton.backgroundColor = [UIColor clearColor];
    [crossButton setImage:[UIImage imageNamed:@"close-128.png"] forState:UIControlStateNormal];
    [crossButton setTintColor:[UIColor blackColor]];
    crossButton.tag = indexPath.row;
    [cell.contentView addSubview:crossButton];
    [crossButton addTarget:self action:@selector(cancelImage:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}

-(void)tblReload
{
    [self.tbl reloadData];
}

#pragma mark - HideKeyboard
-(void)hideKeyB
{
    [commentTV resignFirstResponder];
    [hashTags resignFirstResponder];
}

#pragma mark - Close button
-(IBAction)cancelImage:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    
    [imageCollectionToPost removeObjectAtIndex:i];
    [self.tbl reloadData];
}

@end
