//
//  PhotoViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 12/06/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "PhotoViewController.h"
#import "ContentManager.h"
#import "WebserviceController.h"
#import "CommentsViewController.h"
#import "PicViewController.h"
#import "UserProfileViewController.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "LikesVC.h"
#import "UIImageView+AFNetworking.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
{
    BOOL checkHot;
    BOOL checkCom;
    BOOL webCall;
    UIImage *imgData;
    AppDelegate *appDelegate;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    objManager = [ContentManager sharedManager];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSLog(@"Dictionary : %@",_userDetails);
    
    // SettingProfile Image
    self.navigationItem.title = [_userDetails valueForKey:@"hashtag"];
    
    totalComments.text = [[_userDetails valueForKey:@"comms"] stringByAppendingString:@" Comments"];
    totalLikes.text = [[_userDetails valueForKey:@"hots"] stringByAppendingString:@" Likes"];
    if ([[_userDetails valueForKey:@"comms"] isEqualToString:@"0"]) {
        totalComments.text = @"Comments";
    }
    else
    {
        totalComments.text = [[_userDetails valueForKey:@"comms"] stringByAppendingString:@" Comments"];
    }
    if ([[_userDetails valueForKey:@"hots"] isEqualToString:@"0"]) {
        totalLikes.text = @"Likes";
    }
    else
    {
        totalLikes.text = [[_userDetails valueForKey:@"hots"] stringByAppendingString:@"Likes"];
    }
    
    checkCom = [[_userDetails valueForKey:@"didcom"] boolValue];
    checkHot = [[_userDetails valueForKey:@"didhot"] boolValue];
    
    when.text = [_userDetails valueForKey:@"when"];
    
    userNamez.text = [_userDetails valueForKey:@"n"];
    
    votedUp.hidden = YES;
    
    userProfilePic.layer.masksToBounds = YES;
    userProfilePic.layer.cornerRadius = 35;
    
    //
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    BOOL isDirectory;
    
    NSArray *a = [[_userDetails valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
    NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
    {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
        [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
        else
            NSLog(@"Username folder created");
    }
    
    //assume there is a memoryCache for images or videos
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=userImage.center;
    
    [activityView startAnimating];
    
    [self.view addSubview:activityView];
    
    
    // Downloading Profile Picture
    NSString *imgURL = [_userDetails valueForKey:@"pimg"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Creating User thumb image
    [userProfilePic setImageWithURL:[NSURL URLWithString:imgURL]];
    
    // Download image
    NSArray *tempAr = [[_userDetails valueForKey:@"img"] componentsSeparatedByString:@"/"];
    
    NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
    
    NSString *imgURL2 = [_userDetails valueForKey:@"img"];
    NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
    
    if(![fileManager fileExistsAtPath:writablePath2]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *error = nil;
            [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
            
            if (error) {
                NSLog(@"Error Writing File : %@",error);
                userImage.image = nil;
                imgData = nil;
            }else{
                NSLog(@"Image %@ Saved SuccessFully",imgName2);
                
                userImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
                imgData = userImage.image;
                [activityView stopAnimating];
                [activityView removeFromSuperview];
            }
        });
    }
    else{
        // file exist
        NSLog(@"file exist");
       userImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
        [activityView stopAnimating];
        [activityView removeFromSuperview];
        imgData = userImage.image;
    }
    
    
    // tap gesture
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomMyPhoto)];
    userImage.userInteractionEnabled = YES;
    imageTap.numberOfTapsRequired = 2;
    [userImage addGestureRecognizer:imageTap];
   
}

#pragma mark - Photo Zoom Controller
-(void)zoomMyPhoto
{
    PicViewController *pic = [[PicViewController alloc] initWithNibName:@"PicViewController" bundle:nil];
    
    pic.img = imgData;
    
    [appDelegate.navController pushViewController:pic animated:YES];
}

#pragma mark - Do voting
-(IBAction)VoteBTN:(id)sender
{
    [self doVote];
}

-(void)doVote
{
    webCall = false;
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        
        NSDictionary *userDetail = [objManager getData:@"user_details"];
        
        NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"pid":[_userDetails valueForKey:@"pid"]};
        
        [webSC uservote:tempCall controller:@"1" method:@"dohot.php"];
}

-(IBAction)goToComment:(id)sender
{
    CommentsViewController *come = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
    
    come.userPicDictionary = _userDetails;
    come.type = @"pid";
    
    [appDelegate.navController pushViewController:come animated:YES];
}

-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    NSLog(@"%@",data);
    if(webCall)
    {
        webCall = FALSE;
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            NSArray *tArray = [data valueForKey:@"posts"];
            NSDictionary *tempD = [tArray objectAtIndex:0];
            _userDetails = tempD;
            
            if ([[tempD valueForKey:@"comms"] isEqualToString:@"0"]) {
                totalComments.text = @"Comments";
            }
            else
            {
                totalComments.text = [[tempD valueForKey:@"comms"] stringByAppendingString:@" Comments"];
            }
            
            NSString *ttl = [tempD valueForKey:@"hots"];
            
            if ([ttl isEqualToString:@"0"]) {
                totalLikes.text = @" Likes";
            }
            else
            {
                totalLikes.text = [[tempD valueForKey:@"hots"] stringByAppendingString:@" Likes"];
            }
            
            checkCom = [[tempD valueForKey:@"didcom"] boolValue];
            checkHot = [[tempD valueForKey:@"didhot"] boolValue];
            
            // Setting
            self.navigationItem.title = [tempD valueForKey:@"hashtag"];
            when.text = [tempD valueForKey:@"when"];
            
            userNamez.text = [tempD valueForKey:@"n"];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            BOOL isDirectory;
            
            NSArray *a = [[tempD valueForKey:@"pimg"] componentsSeparatedByString:@"/"];
            NSString *userDirectory = [documentsDirectory stringByAppendingPathComponent:[a lastObject]];
            
            if(![[NSFileManager defaultManager] fileExistsAtPath:userDirectory isDirectory:&isDirectory] || !isDirectory)
            {
                NSError *error = nil;
                NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                [[NSFileManager defaultManager] createDirectoryAtPath:userDirectory withIntermediateDirectories:YES attributes:attr error:&error];
                if (error)
                    NSLog(@"Error creating directory path: %@", [error localizedDescription]);
                else
                    NSLog(@"Username folder created");
            }
            
            //assume there is a memoryCache for images or videos
            UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            
            activityView.center=userImage.center;
            
            [activityView startAnimating];
            
            [self.view addSubview:activityView];
            
            
            // Downloading Profile Picture
            
            NSString *imgURL = [tempD valueForKey:@"pimg"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [userProfilePic setImageWithURL:[NSURL URLWithString:imgURL]];
            // Creating User thumb image
            
            
            // Download image
            NSArray *tempAr = [[tempD valueForKey:@"img"] componentsSeparatedByString:@"/"];
            
            NSString *imgName2 = [NSString stringWithFormat:@"m%@.jpg",[tempAr lastObject]];
            
            NSString *imgURL2 = [tempD valueForKey:@"img"];
            NSString *writablePath2 = [userDirectory stringByAppendingPathComponent:imgName2];
            
            if(![fileManager fileExistsAtPath:writablePath2]){
                // file doesn't exist
                NSLog(@"file doesn't exist");
                //save Image From URL
                NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL2]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *error = nil;
                    [data writeToFile:[userDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName2]] options:NSAtomicWrite error:&error];
                    
                    if (error) {
                        NSLog(@"Error Writing File : %@",error);
                        userImage.image = nil;
                        imgData = nil;
                    }else{
                        NSLog(@"Image %@ Saved SuccessFully",imgName2);
                        
                        userImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
                        imgData = userImage.image;
                        [activityView stopAnimating];
                        [activityView removeFromSuperview];
                    }
                });
            }
            else{
                // file exist
                NSLog(@"file exist");
                userImage.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",userDirectory,imgName2]];
                [activityView stopAnimating];
                [activityView removeFromSuperview];
                imgData = userImage.image;
            }
        }
    }
    else
    {
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            checkHot = TRUE;
            NSString *toHots;
            
            toHots = [NSString stringWithFormat:@"%@",[data valueForKey:@"hots"]];
            if ([toHots isEqualToString:@"0"]) {
                totalLikes.text = [NSString stringWithFormat:@"Likes"];
            }
            else
            {
                totalLikes.text = [NSString stringWithFormat:@"%@ Likes", toHots];
            }
            votedUp.hidden = NO;
            votedUp.alpha = 1.0f;
            // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
            [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
                // Animate the alpha value of your imageView from 1.0 to 0.0 here
                votedUp.alpha = 0.0f;
            } completion:^(BOOL finished) {
                // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
                votedUp.hidden = YES;
            }];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    webCall = true;
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManager getData:@"user_details"];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"count":@1, @"bbid":@"", @"gid":@"", @"pid":[_userDetails valueForKey:@"pid"],@"uid":@""};
    
    [webSC feedcalldictionary:tempCall controller:@"1" method:@"getfeed.php"];
    [SVProgressHUD show];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

#pragma mark - user profile
- (IBAction)goToProfileView:(id)sender {
    UserProfileViewController *userP = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    
    userP.userProf = _userDetails;
    [appDelegate.navController pushViewController:userP animated:YES];
}

#pragma mark - BBid Likes
-(IBAction)showLikes:(id)sender
{
    LikesVC *lks = [[LikesVC alloc] initWithNibName:@"LIkesVC" bundle:nil];
    lks.swap = [_userDetails valueForKey:@"pid"];
    lks.fftype = @"pid";
    lks.userProf = _userDetails;
    [appDelegate.navController pushViewController:lks animated:YES];
}

@end
