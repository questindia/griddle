//
//  ForgotPasswordViewController.h
//  Griddle
//
//  Created by Sukrit Mehra on 24/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceController.h"

@interface ForgotPasswordViewController : UIViewController <UITextFieldDelegate, WebserviceDelegate>

@property (nonatomic, strong) IBOutlet UITextField *username;

-(IBAction)forgertBtnPressed:(id)sender;

@end
