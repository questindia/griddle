//
//  ContactCheckerViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 21/07/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "ContactCheckerViewController.h"
#import "AppDelegate.h"
#import "AFNetworking/AFNetworking.h"
#import "SVProgressHUD.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ContentManager.h"

@interface ContactCheckerViewController ()

@end

@implementation ContactCheckerViewController
{
    NSString *globalCall;
    NSMutableArray *friendlists;
    AppDelegate *appDelegate;
}
@synthesize table, friendList, em, ph, emName, phName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Header.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigate-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(OnClick_btnBack:)];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    [self.navigationItem setTitle:@"Contact List"];
    objManger = [ContentManager sharedManager];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [SVProgressHUD showWithStatus:@"Checking Contact on Griddle" maskType:SVProgressHUDMaskTypeGradient];
    globalCall = @"getContacts";
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    if (ph.count == 0 && em.count == 0) {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MESSAGE!" message:@"No contact found" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    else
    {
    
    NSDictionary *userDetail = [objManger getData:@"user_details"];
    NSDictionary *JSON = @{@"mobile":ph,@"email":em};
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"json":JSON};
    
    [webSC contactChecker:tempCall controller:@"1" method:@"ckall.php"];
    }
    
    friendlists = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate.navController.navigationBar setHidden:NO];
}

#pragma mark - table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(friendlists.count == 0)
        return 0;
    else
        return friendlists.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FBFriends";
    
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    NSDictionary *art = [friendlists objectAtIndex:indexPath.row];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, table.frame.size.width -90, 35)];
    [cell.contentView addSubview:textLabel];
    textLabel.text = [art valueForKey:@"name"];
    
    // Add Remove Button
    UIButton *addRemoveFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addRemoveFriendButton.frame = CGRectMake(table.frame.size.width -60, 10.0f, 50.0f, 35.0f);
    addRemoveFriendButton.backgroundColor = [UIColor clearColor];
    [addRemoveFriendButton setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
    addRemoveFriendButton.tag = indexPath.row;
    [cell addSubview:addRemoveFriendButton];
    [addRemoveFriendButton addTarget:self action:@selector(addRemoveFriend:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (IBAction)addRemoveFriend:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger i = button.tag;
    UIImage *img = [(UIButton *)sender currentImage];
    NSLog(@"Add friend. %ld",(long)i);
    
    WebserviceController *webSC = [[WebserviceController alloc] init];
    webSC.delegate = self;
    
    NSDictionary *userDetail = [objManger getData:@"user_details"];
    
    
    NSString *actionType = @"follow";
    if(img == [UIImage imageNamed:@"addfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addedfriend.png"] forState:UIControlStateNormal];
        actionType = @"follow";
    }
    else if (img == [UIImage imageNamed:@"addedfriend.png"])
    {
        [button setImage:[UIImage imageNamed:@"addfriend.png"] forState:UIControlStateNormal];
        actionType = @"unfollow";
    }
    
    NSDictionary *art = [friendlists objectAtIndex:i];
    
    NSDictionary *tempCall = @{@"user":[userDetail valueForKey:@"user"], @"pass":[userDetail valueForKey:@"pass"], @"target":[art valueForKey:@"uid"], @"action":actionType};
    globalCall = @"friends";
    [webSC friendManager:tempCall controller:@"1" method:@"frmanage.php"];
}

#pragma mark - Webservice Delegate
-(void)webserviceCallback:(NSDictionary *)data
{
    NSLog(@"%@",data);
    if ([globalCall isEqualToString:@"getContacts"])
    {
        if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
        {
            NSArray *matches = [data valueForKey:@"matches"];
            if (matches.count > 0) {
            
                for (NSDictionary *hmna in matches)
                {
                NSString *hash = [hmna valueForKey:@"hash"];
                for (int ij=0; ij<em.count; ij++) {
                    NSString *sa = [em objectAtIndex:ij];
                    
                    if ([sa isEqualToString:hash]) {
                        NSString *name =[emName objectAtIndex:ij];
                        
                        if ([name isEqualToString:@"(null)"]) {
                            name = @"Unknown";
                        }
                        NSDictionary *ads = @{@"name":name, @"uid":[hmna valueForKey:@"uid"]};
                        
                        [friendlists addObject:ads];
                    }
                }
                
                for (int ij=0; ij<ph.count; ij++) {
                    NSString *sa = [ph objectAtIndex:ij];
                    
                    if ([sa isEqualToString:hash]) {
                        NSString *name = [phName objectAtIndex:ij];
                        if ([name isEqualToString:@"(null)"]) {
                            name = @"Unknown";
                        }
                        NSDictionary *ads = @{@"name":name, @"uid":[hmna valueForKey:@"uid"]};
                        
                        [friendlists addObject:ads];
                    }
                }
            }
                
                [table reloadData];
            }
        }
    }
    else if ([globalCall isEqualToString:@"friends"])
    {
    
    }
    [SVProgressHUD dismiss];
}

#pragma mark - Back Button
-(IBAction)OnClick_btnBack:(id)sender  {
    [appDelegate.navController popViewControllerAnimated:YES];
}

@end
