//
//  LikesVC.h
//  Griddle
//
//  Created by Sukrit Mehra on 10/09/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "ViewController.h"
#import "WebserviceController.h"

@class ContentManager;
@interface LikesVC : UIViewController <WebserviceDelegate, UITableViewDataSource, UITableViewDelegate>
{
    ContentManager *ObjManager;
}

@property (nonatomic, strong) IBOutlet UITableView *followerz;

@property (nonatomic, strong) NSString *swap;
@property (nonatomic, strong) NSString *fftype;
@property (nonatomic, strong) NSDictionary *userProf;
@property (nonatomic, strong) NSDictionary *userProfileData;
@end
