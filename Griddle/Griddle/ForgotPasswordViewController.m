//
//  ForgotPasswordViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 24/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
@synthesize username;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [username setDelegate:self];
    
    //Images of username and password
    UIImageView *userImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"User.png"]];
    
    //inserting space from left in uitextfield
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    userImg.frame = CGRectMake(5, 5, 20, 20);
    [spacerView addSubview:userImg];
    [self.username setLeftViewMode:UITextFieldViewModeAlways];
    [self.username setLeftView:spacerView];
    
    UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

-(void)hideKeyBoard
{
    [username resignFirstResponder];
}

#pragma Forget Password
-(IBAction)forgertBtnPressed:(id)sender
{
    UIAlertView *alert;
    if (self.username.text.length ==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Field Empty!" message:@"Username fiels is empty." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        [SVProgressHUD showWithStatus:@"Login in" maskType:SVProgressHUDMaskTypeGradient];
        WebserviceController *webSC = [[WebserviceController alloc] init];
        webSC.delegate = self;
        
        [webSC forget:username.text controller:@"1" method:@"forgot.php"];
    }
}

-(void)webserviceCallback:(NSDictionary *)data
{
    [SVProgressHUD dismiss];
    UIAlertView *alert;
    if([[data valueForKey:@"return"] isEqualToString:@"SUCCESS"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"SUCCESS!" message:@"Reset code is sent on email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [SVProgressHUD dismiss];
        alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:[data valueForKey:@"details"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
@end
