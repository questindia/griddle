
//
//  WebserviceController.m
//  photoshare
//
//  Created by Dhiru on 26/01/14.
//  Copyright (c) 2014 ignis. All rights reserved.
//

#import "WebserviceController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AFNetworking/AFHTTPRequestOperationManager.h"

//static NSString *apiURL = @"http://mobile.griddle.com/api";

//static NSString *apiURL = @"http://www.questglt.com/mobile/griddle2/api";
static NSString *apiURL = @"http://griddle.com/api";

@interface WebserviceController ()

@end

@implementation WebserviceController

@synthesize delegate, receivedData;

// Registration API
-(void) call:(NSDictionary *)postData controller:(NSString *)controller method:(NSString *)method imageData:(NSData *)imageData
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    //[request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"-------SukritMehra-------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    if(imageData != nil)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"; filename=\"%@.jpg\"\r\n", @"pic"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
    }
    else
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"\r\n\r\n%@", [postData valueForKey:@""]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [postData valueForKey:@"user"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [postData valueForKey:@"pass"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"name\"\r\n\r\n%@", [postData valueForKey:@"name"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"mobile\"\r\n\r\n%@", [postData valueForKey:@"mobile"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n%@", [postData valueForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if([postData valueForKey:@"fbtoken"] != nil || [postData valueForKey:@"fbtoken"] != (id)[NSNull null])
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fbtoken\"\r\n\r\n%@", [postData valueForKey:@"fbtoken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if([postData valueForKey:@"fbuid"] != nil || [postData valueForKey:@"fbuid"] != (id)[NSNull null])
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fbuid\"\r\n\r\n%@", [postData valueForKey:@"fbuid"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //initialize an NSURLConnection with the request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
}


// Login API
-(void)logincalldictionary:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method
{
    NSString *poststr = [NSString stringWithFormat:@"username=%@&password=%@&apple_id=%@&fbtoken=%@&fbuid=%@",[call valueForKey:@"username"], [call valueForKey:@"password"], [call valueForKey:@"apple_id"], [call valueForKey:@"fbtoken"],[call valueForKey:@"fbuid"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    [request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *data = [NSData dataWithBytes:[poststr UTF8String] length:[poststr length]];
    
    [request setHTTPBody:data];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
}

// FEED API
-(void)feedcalldictionary:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method
{
    NSString *poststr;
    NSDictionary *afDict;
    NSNumber *check = [call valueForKey:@"pages"];
    if (check == nil)
    {
        poststr = [NSString stringWithFormat:@"username=%@&password=%@&count=%@&bbid=%@&gid=%@&pid=%@&uid=%@&page=%@",[call valueForKey:@"user"], [call valueForKey:@"pass"], [call valueForKey:@"count"],[call valueForKey:@"bbid"], [call valueForKey:@"gid"], [call valueForKey:@"pid"], [call valueForKey:@"uid"], check];
        afDict = @{
                   @"username":[call valueForKey:@"user"],
                   @"password":[call valueForKey:@"pass"],
                   @"count":[call valueForKey:@"count"],
                   @"bbid":[call valueForKey:@"bbid"],
                   @"gid":[call valueForKey:@"gid"],
                   @"pid":[call valueForKey:@"pid"],
                   @"uid":[call valueForKey:@"uid"],
                   @"page":@"",
                   };
    }
    else
    {
        afDict = @{
                   @"username":[call valueForKey:@"user"],
                   @"password":[call valueForKey:@"pass"],
                   @"count":[call valueForKey:@"count"],
                   @"bbid":[call valueForKey:@"bbid"],
                   @"gid":[call valueForKey:@"gid"],
                   @"pid":[call valueForKey:@"pid"],
                   @"uid":[call valueForKey:@"uid"],
                   @"page":[call valueForKey:@"pages"]
                   };
        poststr = [NSString stringWithFormat:@"username=%@&password=%@&count=%@&bbid=%@&gid=%@&pid=%@&uid=%@&page=%@",[call valueForKey:@"user"], [call valueForKey:@"pass"], [call valueForKey:@"count"],[call valueForKey:@"bbid"], [call valueForKey:@"gid"], [call valueForKey:@"pid"], [call valueForKey:@"uid"], [call valueForKey:@"pages"]];
    }
    //
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    [request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *data = [NSData dataWithBytes:[poststr UTF8String] length:[poststr length]];
    
    [request setHTTPBody:data];
//    [NSThread sleepForTimeInterval:1];
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    if(!connection){
//        NSLog(@"Connection Failed");
//    }
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

// POST DATA
-(void)postcall:(NSDictionary *)call controller:(NSString*)controller method:(NSString*)method
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    
    //[request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"-------SukritMehra-------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    NSArray *temp = [call valueForKey:@"pic"];
    
    for(int i=0; i<temp.count;i++)
    {
        if([[temp objectAtIndex:i] isKindOfClass:[UIImage class]])
        {
            //UIImage *image = [self imageWithImage:[temp objectAtIndex:i] scaledToMaxWidth:1024 maxHeight:1024];
            NSData *imageData = UIImageJPEGRepresentation([temp objectAtIndex:i], 0.9);
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic%d\"; filename=\"%@%d.jpg\"\r\n",i+1, @"post",i+1] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
        }
        
    }
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [call valueForKey:@"username"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [call valueForKey:@"password"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sel_grid\"\r\n\r\n%@", [call valueForKey:@"sel_grid"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"typed_grid\"\r\n\r\n%@", [call valueForKey:@"typed_grid"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"message\"\r\n\r\n%@", [call valueForKey:@"message"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    //initialize an NSURLConnection with the request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }

}

// User changes
-(void) userchange:(NSDictionary *)postData controller:(NSString *)controller method:(NSString *)method imageData:(NSData *)imageData
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    //[request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"-------SukritMehra-------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    if(imageData != nil)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"; filename=\"%@.jpg\"\r\n", @"userpic"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
    }
    else
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"\r\n\r\n%@", [postData valueForKey:@""]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [postData valueForKey:@"user"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [postData valueForKey:@"pass"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"name\"\r\n\r\n%@", [postData valueForKey:@"name"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"mobile\"\r\n\r\n%@", [postData valueForKey:@"mobile"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n%@", [postData valueForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // ------
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"mopt\"\r\n\r\n%@", [postData valueForKey:@"mopt"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"eopt\"\r\n\r\n%@", [postData valueForKey:@"eopt"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // New pass
    if([[postData valueForKey:@"newpass"] length] == 0)
    {
    
    }
    else
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"newpass\"\r\n\r\n%@", [postData valueForKey:@"newpass"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //initialize an NSURLConnection with the request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
}

#pragma mark - Vote
-(void)uservote:(NSDictionary *)voteData controller:(NSString *)controller method:(NSString *)method
{

    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[voteData valueForKey:@"user"], @"password":[voteData valueForKey:@"pass"], @"pid":[voteData valueForKey:@"pid"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)uservotes:(NSDictionary *)voteData controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[voteData valueForKey:@"user"], @"password":[voteData valueForKey:@"pass"], @"bbid":[voteData valueForKey:@"bbid"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Get Comments
-(void)getComments:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[call valueForKey:@"user"], @"password":[call valueForKey:@"pass"], @"pid":[call valueForKey:@"pid"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves|NSJSONReadingMutableContainers error:nil];
        if (tempDictionary == nil) {
            NSString *newstr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            newstr = [newstr stringByReplacingOccurrencesOfString:@" \"," withString:@"\","];
            NSData *dataS = [newstr dataUsingEncoding:NSASCIIStringEncoding];
            tempDictionary = [NSJSONSerialization JSONObjectWithData:dataS options:NSJSONReadingMutableLeaves error:nil];
        }
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)getCommentss:(NSDictionary*)call controller:(NSString*)controller method:(NSString*)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[call valueForKey:@"user"], @"password":[call valueForKey:@"pass"], @"bbid":[call valueForKey:@"bbid"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        if (tempDictionary == nil) {
            NSString *newstr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            newstr = [newstr stringByReplacingOccurrencesOfString:@" \"," withString:@"\","];
            NSData *dataS = [newstr dataUsingEncoding:NSASCIIStringEncoding];
            tempDictionary = [NSJSONSerialization JSONObjectWithData:dataS options:NSJSONReadingMutableLeaves error:nil];
        }
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Do Comment
-(void)userComment:(NSDictionary *)commentData controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[commentData valueForKey:@"user"], @"password":[commentData valueForKey:@"pass"], @"pid":[commentData valueForKey:@"pid"], @"comment":[commentData valueForKey:@"comment"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)userComments:(NSDictionary *)commentData controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[commentData valueForKey:@"user"], @"password":[commentData valueForKey:@"pass"], @"bbid":[commentData valueForKey:@"bbid"], @"comment":[commentData valueForKey:@"comment"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Forgot Password
-(void)forget:(NSString *)postData controller:(NSString *)controller method:(NSString *)method
{
    NSString *poststr = [NSString stringWithFormat:@"username=%@",postData];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    [request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *data = [NSData dataWithBytes:[poststr UTF8String] length:[poststr length]];
    
    [request setHTTPBody:data];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
}

#pragma mark - Get friend list by FB
-(void)getFriendList:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method
{
    NSString *poststr = [NSString stringWithFormat:@"username=%@&password=%@",[friends valueForKey:@"user"], [friends valueForKey:@"pass"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
    [request setTimeoutInterval: 2000 ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *data = [NSData dataWithBytes:[poststr UTF8String] length:[poststr length]];
    
    [request setHTTPBody:data];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(!connection){
        NSLog(@"Connection Failed");
    }
}

#pragma mark - Friend Manager to follow/unfollow
-(void)friendManager:(NSDictionary *)frmanager controller:(NSString *)controller method:(NSString *)method
{
    NSNumber *a = [frmanager valueForKey:@"target"];
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[frmanager valueForKey:@"user"], @"password":[frmanager valueForKey:@"pass"], @"target":a, @"action":[frmanager valueForKey:@"action"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Contact Checker
-(void)contactChecker:(NSDictionary *)ckChecker controller:(NSString *)controller method:(NSString *)method
{
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:[ckChecker valueForKey:@"json"] options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
//    NSString *poststr = [NSString stringWithFormat:@"username=%@&password=%@&json=%@",[ckChecker valueForKey:@"user"], [ckChecker valueForKey:@"pass"], jsonString];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method]]];
//    [request setTimeoutInterval: 2000 ];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
//    
//    NSData *data = [NSData dataWithBytes:[poststr UTF8String] length:[poststr length]];
//    
//    [request setHTTPBody:data];
//    
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    if(!connection){
//        NSLog(@"Connection Failed");
//    }
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[ckChecker valueForKey:@"user"], @"password":[ckChecker valueForKey:@"pass"], @"json":jsonString};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)getnotes1:(NSDictionary *)notes controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[notes valueForKey:@"user"], @"password":[notes valueForKey:@"pass"], @"page":[notes valueForKey:@"page"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)getnotes:(NSDictionary *)notes controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[notes valueForKey:@"user"], @"password":[notes valueForKey:@"pass"], @"page":[notes valueForKey:@"page"], @"all":@1};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Get Followers
-(void)getFollowerList:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method
{

    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[friends valueForKey:@"user"], @"password":[friends valueForKey:@"pass"], @"target":[friends valueForKey:@"target"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)getFollowerListbySwap:(NSDictionary *)friends controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[friends valueForKey:@"user"], @"password":[friends valueForKey:@"pass"], @"target":[friends valueForKey:@"target"], @"swap":[friends valueForKey:@"swap"]};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - Show Likes
-(void)getLikeByPID:(NSDictionary *)pidLikes controller:(NSString *)controller method:(NSString *)method
{
    NSInteger i = [[pidLikes valueForKey:@"pid"] integerValue];
    NSNumber *ca = [NSNumber numberWithInteger:i];
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[pidLikes valueForKey:@"user"], @"password":[pidLikes valueForKey:@"pass"], @"pid":ca};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *newstr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)getLikeByBBID:(NSDictionary *)bbidLikes controller:(NSString *)controller method:(NSString *)method
{
    NSInteger i = [[bbidLikes valueForKey:@"bbid"] integerValue];
    NSNumber *ca = [NSNumber numberWithInteger:i];
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[bbidLikes valueForKey:@"user"], @"password":[bbidLikes valueForKey:@"pass"], @"bbid":ca};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *tempDictionary;
        NSString *newstr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        newstr = [newstr stringByReplacingOccurrencesOfString:@" \"," withString:@"\","];
        NSData *dataS = [newstr dataUsingEncoding:NSASCIIStringEncoding];
        tempDictionary = [NSJSONSerialization JSONObjectWithData:dataS options:NSJSONReadingMutableLeaves|NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONWritingPrettyPrinted error:nil];
        
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

-(void)deactive:(NSDictionary *)deact controller:(NSString *)controller method:(NSString *)method
{
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    NSDictionary *afDict = @{@"username":[deact valueForKey:@"user"], @"password":[deact valueForKey:@"pass"], @"commond":@"deactive"};
    
    [operationManager POST:[NSString stringWithFormat:@"%@/%@/%@",apiURL,controller,method] parameters:afDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *tempDictionary;
        NSString *newstr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        newstr = [newstr stringByReplacingOccurrencesOfString:@" \"," withString:@"\","];
        NSData *dataS = [newstr dataUsingEncoding:NSASCIIStringEncoding];
        tempDictionary = [NSJSONSerialization JSONObjectWithData:dataS options:NSJSONReadingMutableLeaves|NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONWritingPrettyPrinted error:nil];
        
        [self.delegate webserviceCallback:tempDictionary];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *tempDictionary = @{@"return":@"ERROR",@"details":@"The request timed out."};
        NSLog(@"%@",error.description);
        [self.delegate webserviceCallback:tempDictionary];
    }];
}

#pragma mark - DataConnection delegate
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    receivedData=data;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection failed with error : %@", error.localizedDescription);
    
    NSDictionary *temp =@{@"details":error.localizedDescription};
    
    [self.delegate webserviceCallback:temp];
}
/*
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
}


-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:
(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod
            isEqualToString:NSURLAuthenticationMethodServerTrust];
}
*/
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:
(NSURLAuthenticationChallenge *)challenge {
    /*
    if (([challenge.protectionSpace.authenticationMethod
          isEqualToString:NSURLAuthenticationMethodServerTrust]) &&
        (YES)) {
        if ([challenge.protectionSpace.host isEqualToString:apiURL]) {
            NSLog(@"Allowing bypass...");
            NSURLCredential *credential = [NSURLCredential credentialForTrust:
                                           challenge.protectionSpace.serverTrust];
            [challenge.sender useCredential:credential
                 forAuthenticationChallenge:challenge];
        }
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];*/
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Request Complete, recieved %lu bytes of data",(unsigned long)receivedData.length);
    //NSString *newstr1 = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",newstr1);
    
    NSDictionary *tempDictionary;
    if(receivedData.length != 0)
    {
        tempDictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:nil];
        if (tempDictionary == nil) {
            NSString *newstr = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            newstr = [newstr stringByReplacingOccurrencesOfString:@" \"," withString:@"\","];
            NSData *dataS = [newstr dataUsingEncoding:NSASCIIStringEncoding];
            tempDictionary = [NSJSONSerialization JSONObjectWithData:dataS options:NSJSONReadingMutableLeaves error:nil];
        }
        
    }
    else
    {
        tempDictionary = nil;
    }
    [self.delegate webserviceCallback:tempDictionary];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    return [self imageWithImage:image scaledToSize:newSize];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end

// [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
