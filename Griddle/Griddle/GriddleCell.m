//
//  GriddleCell.m
//  Changing-image
//
//  Created by ignis3 on 22/05/14.
//  Copyright (c) 2014 ignis3. All rights reserved.
//

#import "GriddleCell.h"

@implementation GriddleCell
@synthesize griddleImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        griddleImage.layer.borderWidth = 0.5;
        griddleImage.layer.borderColor = [UIColor blackColor].CGColor;
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
